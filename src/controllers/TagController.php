<?php namespace WorkInProgress\ClientEcommerce;

class TagController extends \BaseController {

  public function __construct()
  {
    $this->beforeFilter('csrf', array('on' => 'post'));
  }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function show($tag = 'new')
  {
    //Look for a product instead
    $tag = Tag::where('value', '=', $tag)->firstOrFail();

    if($tag) {
      $data = array(
        'category' => $tag,
        'pages' => \WorkInProgress\ClientPages\Page::find(1),
        'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
        'title' => $tag->value
      );

      //Display category
      return \View::make('ecommerce::category', $data);
    }
  }
}

?>
