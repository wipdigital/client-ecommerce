<?php namespace WorkInProgress\ClientEcommerce;

use Facebook\FacebookRequest;
use Facebook\FacebookSession;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Carbon\Carbon;

class CustomerController extends \BaseController {

  private $index_rules = [
    'name' => 'required',
    'email' => 'required|email'
  ];

  private $billing_rules;
  private $shipping_rules;
  private $voucher_rules;
  private $discount_rules;

  public function __construct()
  {
    $this->billing_rules = \Config::get('ecommerce::customer.validation.billing');

    $this->shipping_rules = \Config::get('ecommerce::customer.validation.shipping');

    $this->voucher_rules = \Config::get('ecommerce::customer.validation.voucher');

    $this->discount_rules = \Config::get('ecommerce::customer.validation.discount');

    $this->beforeFilter('csrf', ['on' => 'post']);
  }

  private function getCart() {
    //Update the state of the cart to a customer
    $cart = \App::make('WorkInProgress\ClientEcommerce\CartController')->getCart();
    $cart->state = ($cart->state > 2) ? $cart->state : 2;
    $cart->save();

    return $cart;
  }

  public function getLogout() {
    \Session::forget('customer_id');
    \Session::forget('cart_id');

    return \Redirect::to('/');
  }

  public function getInstagram()
  {
    $url = 'https://api.instagram.com/oauth/access_token';

    $data = [
      'client_id' => \Config::get('ecommerce::customer.instagram.id'),
      'client_secret' => \Config::get('ecommerce::customer.instagram.secret'),
      'grant_type' => 'authorization_code',
      'redirect_uri' => \Config::get('ecommerce::customer.instagram.redirect_uri'),
      'code' => \Input::get('code')
    ];

    $options = [
      'http' => [
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
      ]
    ];

    $context  = stream_context_create($options);
    $response = file_get_contents($url, false, $context);
    $data = json_decode($response);

    $customer = Customer::where('instagram_id', '=', $data->user->id)->first();

    if(!$customer) {
      $customer = Customer::create([
        'instagram_id' => $data->user->id,
        'name' => $data->user->full_name,
        'shipping_name' => $data->user->full_name
      ]);
    }

    $customer->profile_picture = $data->user->profile_picture;
    $customer->save();

    if(\Session::has('cart_id')) {
      $customer->shopping_cart_id = \Session::get('cart_id');
      $customer->save();
    }

    \Session::put('customer_id', $customer->id);

    setcookie('redirect_uri', '', time()-3600);

    $redirect_uri = isset($_COOKIE['redirect_uri']) ? $_COOKIE['redirect_uri'] : '/';
    return \Redirect::to($redirect_uri);
  }

  public function getFacebook()
  {
    $url = "https://graph.facebook.com/v2.3/oauth/access_token?client_id=" . \Config::get('ecommerce::customer.facebook.id') . "&redirect_uri=" . \Config::get('ecommerce::customer.facebook.redirect_uri') . "&client_secret=" . \Config::get('ecommerce::customer.facebook.secret') . "&code=" . \Input::get('code');

    $response = file_get_contents($url);
    $data = json_decode($response);

    $access_token = $data->access_token;
    $session = new FacebookSession($access_token);

    FacebookSession::setDefaultApplication(\Config::get('ecommerce::customer.facebook.id'), \Config::get('ecommerce::customer.facebook.secret'));

    $user_profile = (new FacebookRequest(
      $session, 'GET', '/me'
    ))->execute()->getGraphObject(GraphUser::className());

    $customer = Customer::where('facebook_id', '=', $user_profile->getId())->first();

    if(!$customer) {
      $customer = Customer::create([
        'facebook_id' => $user_profile->getId(),
        'name' => $user_profile->getName(),
        'shipping_name' => $user_profile->getName()
      ]);
    }

    $customer->profile_picture = 'http://graph.facebook.com/' . $user_profile->getId() . '/picture';
    $customer->save();


    if(\Session::has('cart_id')) {
      $customer->shopping_cart_id = \Session::get('cart_id');
      $customer->save();
    }

    \Session::put('customer_id', $customer->id);

    setcookie('redirect_uri', '', time()-3600);

    $redirect_uri = isset($_COOKIE['redirect_uri']) ? $_COOKIE['redirect_uri'] : '/';
    return \Redirect::to($redirect_uri);
  }

  public function getIndex()
  {
    $cart = $this->getCart();

    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'title' => 'Checkout',
    ];

    \Session::forget('same_as_billing');

    return \View::make('ecommerce::cart.checkout', $data);
  }

  public function postIndex()
  {
    $cart = $this->getCart();

    //Trim and convert customer details to uppercase
    $data = array_map('trim', \Input::all());
    $data = array_map('strtoupper', $data);

    $validator = \Validator::make($data, $this->index_rules);

    if($validator->fails()) {
      return \Redirect::to('checkout')->withErrors($validator)->withInput();
    }

    $customer = Customer::create($data);

    if(\Session::has('cart_id')) {
      $customer->shopping_cart_id = \Session::get('cart_id');
    }

    $customer->shipping_name = $customer->name;
    $customer->shipping_email = $customer->email;

    $customer->save();

    \Session::put('customer_id', $customer->id);

    $redirect_uri = \Input::has('redirect_uri') ? \Input::get('redirect_uri') : '/checkout/billing';
    return \Redirect::to($redirect_uri);
  }

  public function getBilling()
  {
    if(!\Session::has('cart_id')) {
      return \Redirect::to('/');
    }

    $cart = $this->getCart();

    if(!$cart->customer) {
      return \Redirect::to('checkout');
    }

    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'title' => 'Billing',
      'customer' => $cart->customer
    ];

    return \View::make('ecommerce::cart.billing', $data);
  }

  public function postBilling()
  {
    if(!\Session::has('cart_id')) {
      return \Redirect::to('cart');
    }

    $cart = $this->getCart();

    //Trim and convert customer details to uppercase
    $data = array_map('trim', \Input::all());
    $data = array_map('strtoupper', $data);

    $rules = $this->billing_rules;
    //If location_control is enabled check to see if postcode and suburb exist in the locations table
    if(\Config::get('ecommerce::shipping.location_control')) {
      $rules['suburb'] = 'required|exists:locations,suburb,postcode,' . ltrim($data['postcode'], '0');
      $messages = [
        'suburb.exists' => 'The :attribute does not match the postcode.'
      ];
    }

    $validator = \Validator::make($data, $rules, isset($messages) ? $messages : []);

    if($validator->fails()) {
      return \Redirect::to('checkout/billing')->withErrors($validator)->withInput();
    }

    $cart->customer->update($data);

    $redirect_uri = \Input::has('redirect_uri') ? \Input::get('redirect_uri') : '/checkout/shipping';
    return \Redirect::to($redirect_uri);
  }

  public function getShipping()
  {
    if(!\Session::has('cart_id')) {
      return \Redirect::to('/');
    }

    $cart = $this->getCart();

    if(!$cart->customer) {
      return \Redirect::to('checkout');
    }

    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'title' => 'Shipping',
      'customer' => $cart->customer
    ];

    return \View::make('ecommerce::cart.shipping', $data);
  }

  public function postShipping()
  {
    if(!\Session::has('cart_id')) {
      return \Redirect::to('cart');
    }

    $cart = $this->getCart();

    //Trim and convert customer details to uppercase
    $data = array_map('trim', \Input::all());
    $data = array_map('strtoupper', $data);

    if(\Input::has('same_as_billing')) {
      \Session::put('same_as_billing', true);

      $data['shipping_name'] = $cart->customer->name;
      $data['shipping_email'] = $cart->customer->email;
      $data['shipping_phone'] = $cart->customer->phone;
      $data['shipping_address1'] = $cart->customer->address1;
      $data['shipping_address2'] = $cart->customer->address2;
      $data['shipping_city'] = $cart->customer->city;
      $data['shipping_suburb'] = $cart->customer->suburb;
      $data['shipping_state'] = $cart->customer->state;
      $data['shipping_country'] = $cart->customer->country;
      $data['shipping_postcode'] = $cart->customer->postcode;

      unset($data['same_as_billing']);
    } else {
      dd('1');
      //todo we need to unset this for subsequent requests
      \Session::put('same_as_billing', false);
    }

    //Subscribe the user to the mailing list
    if(isset($data['subscribe'])) {
      $json = [];
      $json[\Config::get('ecommerce::subscribe.name')] = $cart->customer->email;

      $client = new \GuzzleHttp\Client();
      $response = $client->post(\Config::get('ecommerce::subscribe.endpoint'), [
        'body' => $json
      ]);

    }
    unset($data['subscribe']);

    $rules = $this->shipping_rules;
    //If location_control is enabled check to see if postcode and suburb exist in the locations table
    if(\Config::get('ecommerce::shipping.location_control')) {
      $rules['shipping_suburb'] = 'required|exists:locations,suburb,postcode,' . ltrim($data['postcode'], '0');
      $messages = [
        'shipping_suburb.exists' => 'The :attribute does not match the postcode.'
      ];
    }

    $validator = \Validator::make($data, $rules, isset($messages) ? $messages : []);

    if($validator->fails()) {
      return \Redirect::to('checkout/shipping')->withErrors($validator)->withInput();
    }

    $cart->customer->update($data);

    $redirect_uri = \Input::has('redirect_uri') ? \Input::get('redirect_uri') : '/payment/creditcard';
    return \Redirect::to($redirect_uri);
  }

  public function getSummary()
  {
    if(!\Session::has('cart_id')) {
      return \Redirect::to('/');
    }

    $cart = $this->getCart();

    if(!$cart->customer) {
      return \Redirect::to('checkout');
    }

    $total = 0.00;
    foreach($cart->products as $product) {
      $total += $product->pivot->quantity * ($product->price + $product->pivot->price_offset);
    }

    $gst = $total / 11;

    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'title' => 'Checkout',
      'customer' => $cart->customer,
      'discount' => $cart->discountCode ?: new DiscountCode,
      'total' => $total,
      'gst' => $gst
    ];

    return \View::make('ecommerce::cart.summary', $data);
  }

  public function postSummary()
  {
    if(!\Session::has('cart_id')) {
      return \Redirect::to('cart');
    }

    $cart = $this->getCart();

    $data = \Input::all();

    $validator = \Validator::make($data, $this->voucher_rules);

    if($validator->fails()) {
      $validator = \Validator::make($data, $this->discount_rules);

      if($validator->fails()) {
        return \Redirect::to('checkout/summary')->withErrors($validator)->withInput();
      }

      $discount_code = DiscountCode::where('code', '=', $data['code'])->first();

      $cart->discount_code_id = $discount_code->id;
      $cart->save();

    } else {
      $voucher = Voucher::where('code', '=', $data['code'])->first();

      $cart->voucher_id = $voucher->id;
      $cart->save();
    }

    return \Redirect::to('checkout/summary');
  }

  public function getTime()
  {
    if(!\Session::has('cart_id')) {
      return \Redirect::to('/');
    }

    $cart = $this->getCart();

    if(!$cart->customer) {
      return \Redirect::to('checkout');
    }

    $times = [];
    $times['ASAP'] = 'ASAP';

    $now = Carbon::now();
    $now->hour += 1;

    $timestamp_end = Carbon::today();
    $timestamp_end->hour = 22;
    $timestamp_end->minute = 30;

    $timestamp_start = Carbon::today();
    $timestamp_start->hour = 17;
    $timestamp_start->minute = 30;

    for($i=0; $i<24; $i++) {
      $timestamp = Carbon::today();
      $timestamp->hour += $i;

      for($y=0; $y<2; $y++) {
        $timestamp->minute += 30;

        if($timestamp->gte($now) && $timestamp->gte($timestamp_start) && $timestamp->lte($timestamp_end)) {
          $times[$timestamp->format('h:i A')] = $timestamp->format('h:i A');
        }
      }
    }

    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'title' => 'Select a time',
      'customer' => $cart->customer,
      'times' => $times
    ];

    return \View::make('ecommerce::cart.time', $data);
  }

  public function postTime()
  {
    if(!\Input::has('timestamp')) {
      return \Redirect::to('checkout/time');
    }

    \Session::put('delivery_method', \Input::get('delivery_method'));
    \Session::put('delivery_time', \Input::get('timestamp'));

    $redirect_uri = \Input::has('redirect_uri') ? \Input::get('redirect_uri') : '/checkout/billing';
    return \Redirect::to($redirect_uri);
  }

}
