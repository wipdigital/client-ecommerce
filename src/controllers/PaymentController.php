<?php namespace WorkInProgress\ClientEcommerce;

use Omnipay\Omnipay;
use Carbon\Carbon;

class PaymentController extends \BaseController {

  private $kounta_token;

  private $commbank_rules = [
    'vpc_Version' => 'required',
    'vpc_Command' => 'required',
    'vpc_AccessCode' => 'required',
    'vpc_MerchTxnRef' => 'required',
    'vpc_Merchant' => 'required',
    'vpc_OrderInfo' => 'required',
    'vpc_Amount' => 'required',
    'vpc_CardNum' => 'required|numeric',
    'vpc_CardName' => 'required',
    'vpc_CardExpYear' => 'required|numeric',
    'vpc_CardExpMonth' => 'required|numeric',
    'vpc_CardSecurityCode' => 'required|numeric'
  ];

  private $voucher_rules;
  private $discount_rules;


  public function __construct()
  {
    $this->beforeFilter('csrf', ['on' => 'post']);

    $this->voucher_rules = \Config::get('ecommerce::customer.validation.voucher');

    $this->discount_rules = \Config::get('ecommerce::customer.validation.discount');
  }

  private function getCart() {
    //Update the state of the cart to a customer
    $cart = \App::make('WorkInProgress\ClientEcommerce\CartController')->getCart();
    $cart->state = ($cart->state > 3) ? $cart->state : 3;
    $cart->save();

    return $cart;
  }

  private function processSale($cart, $payment_method = 'cod', $return_json = false) {
    $payment_method = 'commbank';
    $delivery_method = \Session::has('delivery_method') ? \Session::get('delivery_method') : '';

    $sale = Sale::create([
      'total' => $cart->total,
      'customer_id' => $cart->customer->id,
      'discount_code_id'=> $cart->discount_code_id,
      'discount' => $cart->total_discount,
      'voucher_id' => $cart->voucher_id,
      'voucher' => $cart->total_voucher,
      'shipping' => $cart->total_shipping,
      'email' => $cart->customer->email,
      'name' => $cart->customer->name,
      'phone' => $cart->customer->phone,
      'address1' => $cart->customer->address1,
      'address2' => $cart->customer->address2,
      'city' => $cart->customer->city,
      'suburb' => $cart->customer->suburb,
      'state' => $cart->customer->state,
      'country' => $cart->customer->country,
      'postcode' => $cart->customer->postcode,
      'shipping_email' => $cart->customer->shipping_email,
      'shipping_name' => $cart->customer-shipping_>name,
      'shipping_phone' => $cart->customer->shipping_phone,
      'shipping_address1' => $cart->customer->shipping_address1,
      'shipping_address2' => $cart->customer->shipping_address2,
      'shipping_city' => $cart->customer->shipping_city,
      'shipping_suburb' => $cart->customer->shipping_suburb,
      'shipping_state' => $cart->customer->shipping_state,
      'shipping_country' => $cart->customer->shipping_country,
      'shipping_postcode' => $cart->customer->shipping_postcode,
      'payment_method' => $payment_method,
      'delivery_method' => $delivery_method
    ]);

    $sale->save();

    //Update voucher amount remaining
    if($cart->voucher) {
      $voucher = Voucher::find($cart->voucher_id);
      $voucher->amount -= $cart->total_voucher;
      $voucher->save();
    }

    //Archive current product information
    foreach($cart->products as $product) {
      $stock_id = null;

      if(\Config::get('ecommerce::product.stock_control') && !$product->virtual) {
        $colour_id = $product->pivot->options()->colour()->first() ? $product->pivot->options()->colour()->first()->id : 0;
        $size_id = $product->pivot->options()->size()->first() ? $product->pivot->options()->size()->first()->id : 0;

        if($colour_id && $size_id) {
          $stock_query = ProductOptionStock::whereRaw("colour_id = ? and size_id = ?", [$colour_id, $size_id]);
        } else if($colour_id || $size_id) {
          $stock_query = ProductOptionStock::whereRaw("(size_id is null and colour_id = ?) or (colour_id is null and size_id = ?)", [$colour_id, $size_id]);
        } else {
          $stock_query = ProductOptionStock::whereRaw("colour_id is null and size_id is null and product_id = ?", [$product->id]);
        }

        if($stock_query->count()) {
          $stock = $stock_query->first();
          $stock_id = $stock->id;

          ProductOptionStockLog::create([
            'product_id' => $product->id,
            'product_option_stock_id' => $stock_id,
            'type' => 0,
            'quantity' => -$product->pivot->quantity
          ]);

          $stock->quantity -= $product->pivot->quantity;
          $stock->save();
        }
      }

      $sale_product = SaleProduct::create([
        'title' => $product->title,
        'short_description' => $product->short_description,
        'price' => $product->price + $product->pivot->price_offset,
        'quantity' => $product->pivot->quantity,
        'thumbnail' => $product->images()->first() ? $product->images()->first()->src : 0,
        'product_id' => $product->id,
        'product_option_stock_id' => $stock_id
      ]);

      //Attach the product options
      foreach($product->pivot->options()->get() as $option) {
        $sale_product->options()->attach($option->id);
      }

      $sale->products()->save($sale_product);

      //Create gift card if the product is virtual
      if($product->virtual) {
        $code = str_random(10);

        $voucher = Voucher::create([
          'title' => $product->full_title,
          'short_description' => $product->short_description,
          'code' => $code,
          'amount' => $product->price,
          'active' => true
        ]);

        //Email voucher to client
        $data = [
          'cart' => $cart,
          'sale' => $sale,
          'voucher' => $voucher
        ];

        \Mail::send('ecommerce::emails.cart.payment.voucher', $data, function($message) use ($sale) {
          $message->to($sale->shipping_email, $sale->shipping_name)->subject(\Config::get('ecommerce::payment.email.voucher_subject'));
        });
      }
    }

    $this->processXero($sale);
    $this->processKounta($cart, $sale);

    $data = [
      'cart' => $cart,
      'sale' => $sale
    ];

    \Mail::send('ecommerce::emails.cart.payment.' . $payment_method, $data, function($message) use ($sale) {
      $message->to($sale->shipping_email, $sale->shipping_name)->subject(\Config::get('ecommerce::payment.email.subject'));
    });

    $settings = [
      'site_name' => \DB::table('settings')->where('key', '=', 'site_name')->pluck('value'),
      'site_email' => \DB::table('settings')->where('key', '=', 'site_email')->pluck('value')
    ];

    \Mail::send('ecommerce::emails.cart.payment.site', $data, function($message) use ($settings) {
      $message->to($settings['site_email'], $settings['site_name'])->subject(\Config::get('ecommerce::payment.email.subject'));
    });

    $cart->customer->last_shopping_cart_id = $cart->id;
    $cart->customer->save();
    $cart->state = 4;
    $cart->save();

    \Session::forget('cart_id');
    \Session::forget('payment_method');
    \Session::forget('delivery_method');
    \Session::forget('same_as_billing');
    \Session::put('sale_id', $sale->id);

    return $return_json ? json_encode(['status' => 'OK']) : \Redirect::to('payment/successful');
  }

  private function getKountaToken()
  {
    if($this->kounta_token) {
      return $this->kounta_token;
    }

    $client = new \GuzzleHttp\Client();

    $response = $client->post(\Config::get('ecommerce::kounta.endpoint.access_token'), [
      'body' => [
        'refresh_token' => \Setting::where('key', 'kounta_refresh_token')->firstOrFail()->value,
        'client_id' => \Config::get('ecommerce::kounta.client_id'),
        'client_secret' => \Config::get('ecommerce::kounta.client_secret'),
        'grant_type' => 'refresh_token'
      ]
    ]);

    if($response->getStatusCode() == 200) {
      $json = json_decode($response->getBody()->getContents());

      $this->kounta_token = $json->access_token;

      return $json->access_token;
    }

  }

  private function processKounta($cart, $sale)
  {
    if(\Config::get('ecommerce::kounta.active')) {
      $client = new \GuzzleHttp\Client();

      $response = $client->get(\Config::get('ecommerce::kounta.endpoint.base') . '/companies/' . \Setting::where('key', 'kounta_company_id')->firstOrFail()->value . '/products.json', [
        'headers' => [
          'Authorization' => 'Bearer ' . $this->getKountaToken()
        ]
      ]);

      if($response->getStatusCode() == 200) {
        $kounta_products = json_decode($response->getBody()->getContents());

        if($response->getHeader('X-Next-Page')) {
          $url = $response->getHeader('X-Next-Page');

          $x = 0;
          while($x < 4) {
            $x++;
            $response = $client->get($url, [
              'headers' => [
                'Authorization' => 'Bearer ' . $this->getKountaToken()
              ]
            ]);

            if($response->getStatusCode() == 200) {
              $extra_products = json_decode($response->getBody()->getContents());
              $kounta_products = array_merge($kounta_products, $extra_products);

              $url = $response->getHeader('X-Next-Page') ?: null;
            }
          }
        }

        if($response->getStatusCode() == 200) {
          //we need the customers email in order to create order
          $email = $sale->shipping_email ?: $sale->email;
          if($email) {
            $customer = [];
              $customer['primary_email_address'] = $email;

            $name = $sale->shipping_name ?: $sale->name;

            $name = explode(' ', trim($name));
            $first_name = $name[0];
            $customer['first_name'] = $first_name;

            unset($name[0]);
            $last_name = implode(' ', $name);
            if($last_name) {
              $customer['last_name'] = $last_name;
            }

            $phone = $sale->shipping_phone ?: $sale->phone;
            if($phone) {
              $customer['phone'] = $phone;
            }

            $address = count($sale->address1) ? $sale->address1 . ' ' . $sale->address2 . ', ' . $sale->suburb :
                                                $sale->shipping_address1 . ' ' . $sale->shipping_address2 . ', ' . $sale->shipping_suburb;

            $postal_code = $sale->shipping_postcode ?: $sale->postcode;

            //try finding the customer from their email otherwise create a new customer
            try {
              $response = $client->get(\Config::get('ecommerce::kounta.endpoint.base') . '/companies/' . \Setting::where('key', 'kounta_company_id')->firstOrFail()->value . '/customers.json?email=' . $email, [
                'headers' => [
                  'Authorization' => 'Bearer ' . $this->getKountaToken()
                ]
              ]);

              if($response->getStatusCode() == 200) {
                $kounta_customer = json_decode($response->getBody()->getContents());
              }
            } catch(\GuzzleHttp\Exception\RequestException $e) {
              try {
                $response = $client->post(\Config::get('ecommerce::kounta.endpoint.base') . '/companies/' . \Setting::where('key', 'kounta_company_id')->firstOrFail()->value . '/customers.json', [
                  'headers' => [
                    'Authorization' => 'Bearer ' . $this->getKountaToken()
                  ],
                  'json' => $customer
                ]);

                if($response->getStatusCode() <= 201) {
                  $response = $client->get(\Config::get('ecommerce::kounta.endpoint.base') . '/companies/' . \Setting::where('key', 'kounta_company_id')->firstOrFail()->value . '/customers.json?email=' . $email, [
                    'headers' => [
                      'Authorization' => 'Bearer ' . $this->getKountaToken()
                    ]
                  ]);

                  if($response->getStatusCode() == 200) {
                    $kounta_customer = json_decode($response->getBody()->getContents());
                  }
                }
              } catch(\GuzzleHttp\Exception\RequestException $e) {
                \Log::error(print_r($customer, 1));
                \Log::error(print_r($e->getResponse()->getBody()->getContents(), 1));
                \Log::error(print_r($e->getMessage(), 1));
              }
            }

            unset($customer['primary_email_address']);

            switch($sale->delivery_method) {
              case 'delivery':
                if($address) {
                  $customer['primary_address'] = [
                    'address' => $address,
                    'city' => 'Perth',
                    'state' => 'WA',
                    'postal_code' => $postal_code,
                    'country' => 'AU'
                  ];

                  $customer['shipping_address'] = $customer['primary_address'];
                }
              break;
            }

            try {
              $response = $client->put(\Config::get('ecommerce::kounta.endpoint.base') . '/companies/' . \Setting::where('key', 'kounta_company_id')->firstOrFail()->value . '/customers/' . $kounta_customer->id . '.json', [
                'headers' => [
                  'Authorization' => 'Bearer ' . $this->getKountaToken()
                ],
                'json' => $customer
              ]);
            } catch(\Exception $e) {
              \Log::error(print_r($customer, 1));
              \Log::error(print_r($e->getResponse()->getBody()->getContents(), 1));
              \Log::error(print_r($e->getMessage(), 1));
            }

            $kounta_order = [
              'site_id' => (int)\Setting::where('key', 'kounta_site_id')->firstOrFail()->value,
              'customer_id' => (int)$kounta_customer->id,
              'status' => 'SUBMITTED',
              'lines' => []
            ];

            $kounta_order['notes'] = '';
            if($sale->payment_method == 'creditcard') {
              $kounta_order['payments'] = [];
              $kounta_order['payments'][] = [
                'method_id' => \Config::get('ecommerce::kounta.method_id'),
                'amount' => $cart->total
              ];

              $kounta_order['notes'] .= 'PAID. ';
            } else {
              $kounta_order['notes'] .= 'UNPAID. ';
            }

            switch($sale->delivery_method) {
              case 'pickup':
                $kounta_order['notes'] .= 'Pickup order at ' . \Session::get('delivery_time') . ' for ' . $name . ' (' . $phone . ')';
                break;

              case 'delivery':
                $kounta_order['notes'] .= 'Delivery order at ' . \Session::get('delivery_time') . ' for ' . $name . ' (' . $phone . ') to ' . $address;
                break;
            }

            foreach($cart->products as $cart_product) {
              $kounta_product = array_first($kounta_products, function($key, $value) use ($cart_product) {
                return $value->name == $cart_product->title;
              });

              if($kounta_product) {
                $line = [
                  'product_id' => $kounta_product->id,
                  'quantity' => (int)$cart_product->pivot->quantity,
                ];

                if($cart_product->pivot->options()->variation()->count()) {
                  $line['modifiers'] = [];

                  foreach($cart_product->pivot->options()->variation()->get() as $option) {
                    $kounta_modifier = array_first($kounta_products, function($key, $value) use ($option) {
                      return $value->name == 'ADD ' . $option->title;
                    });

                    $line['modifiers'][] = $kounta_modifier->id;
                  }

                }

                $kounta_order['lines'][] = $line;
              }

            }

            try {
              $response = $client->post(\Config::get('ecommerce::kounta.endpoint.base') . '/companies/' . \Setting::where('key', 'kounta_company_id')->firstOrFail()->value . '/orders.json', [
                'headers' => [
                  'Authorization' => 'Bearer ' . $this->getKountaToken(),
                ],
                'json' => $kounta_order
              ]);
            } catch(\Exception $e) {
              \Log::error(print_r($kounta_order, 1));
              \Log::error(print_r($e->getResponse()->getBody()->getContents(), 1));
              \Log::error(print_r($e->getMessage(), 1));
            }


            if($response->getStatusCode() == 201) {
              return true;

            }

          }

        }

      }

    }

    return false;
  }

  private function processXero($sale)
  {
    if(\Config::get('ecommerce::xero.active')) {
      $invoice = [
        [
          'Type' => 'ACCREC',
          'Contact' => [
            'Name' => $sale->name,
            'EmailAddress' => $sale->email
          ],
          'LineItems' => [
            'LineItem' => []
          ],
          'Date' => Carbon::now()->format('Y-m-d'),
          'DueDate' => Carbon::now()->format('Y-m-d'),
          'LineAmountTypes' => 'Inclusive',
          'Status' => 'AUTHORISED',
          'SentToContact' => 'true',
        ]
      ];

      foreach($sale->products as $product) {
        $title = $product->product->full_title;

        if($product->options()->count()) {
          if($product->options()->colour()->count()) {
            $title .= ' ' . $product->options()->colour()->first()->title;
          }
          if($product->options()->size()->count()) {
            $title .= ' ' . $product->options()->size()->first()->title;
          }
        }

        if($sale->discount_code && $sale->discount_code->type == 1) {
          $line_item = [
            'Description' => $title,
            'Quantity' => $product->quantity,
            'DiscountRate' => $sale->discount_code->amount,
            'UnitAmount' => $product->price,
            'AccountCode' => \Config::get('ecommerce::xero.sales_account_code'),
          ];

          /*if(isset($product->stock)) {
            $line_item['ItemCode'] = $product->stock->sku;
          }*/

          $invoice[0]['LineItems']['LineItem'][] = $line_item;
        } else if($sale->discount_code && $sale->discount_code->type == 4 && $product->product->tags->contains(4)) {
          $line_item = [
            'Description' => $title,
            'Quantity' => $product->quantity,
            'DiscountRate' => 50,
            'UnitAmount' => $product->price,
            'AccountCode' => \Config::get('ecommerce::xero.sales_account_code'),
          ];

          /*if(isset($product->stock)) {
            $line_item['ItemCode'] = $product->stock->sku;
          }*/

          $invoice[0]['LineItems']['LineItem'][] = $line_item;
        } else {
          $line_item = [
            'Description' => $product->product->full_title,
            'Quantity' => $product->quantity,
            'UnitAmount' => $product->price,
            'AccountCode' => \Config::get('ecommerce::xero.sales_account_code'),
          ];

          /*if(isset($product->stock)) {
            $line_item['ItemCode'] = $product->stock->sku;
          }*/

          $invoice[0]['LineItems']['LineItem'][] = $line_item;
        }
      }

      // Process % of Shipping Total discount
      if($sale->shipping) {
        if($sale->discount_code && $sale->discount_code->type == 3) {
          $invoice[0]['LineItems']['LineItem'][] = [
            'Description' => 'Shipping',
            'Quantity' => 1,
            'DiscountRate' => $sale->discount_code->amount,
            'UnitAmount' => $sale->shipping,
            'AccountCode' => \Config::get('ecommerce::xero.sales_account_code'),
          ];
        } else {
          $invoice[0]['LineItems']['LineItem'][] = [
            'Description' => 'Shipping',
            'Quantity' => 1,
            'UnitAmount' => $sale->shipping,
            'AccountCode' => \Config::get('ecommerce::xero.sales_account_code'),
          ];
        }
      }

      $result = \XeroLaravel::Invoices($invoice);
      \Log::info(print_r($result,1));

      $contact_id = $result['Invoices']['Invoice']['Contact']['ContactID'];
      $invoice_number = $result['Invoices']['Invoice']['InvoiceNumber'];
      $invoice_id = $result['Invoices']['Invoice']['InvoiceID'];

      $sale->invoice_id = $invoice_number;
      $sale->save();

      $invoice = [
        [
          'Type' => 'ACCPAY',
          'Contact' => [
            'Name' => 'Stock Adjustment'
          ],
          'Date' => Carbon::now()->format('Y-m-d'),
          'DueDate' => Carbon::now()->format('Y-m-d'),
          'LineAmountTypes' => 'NoTax',
          'Status' => 'AUTHORISED',
          'LineItems' => [
            'LineItem' => []
          ]
        ]
      ];

      //todo update UnitAmount to take into account true value
      foreach($sale->products as $product) {
        $invoice[0]['LineItems']['LineItem'][] = [
          'Description' => 'Sale of Good - Increase COGS',
          'Quantity' => $product->quantity,
          'UnitAmount' => 1.78,
          'AccountCode' => \Config::get('ecommerce::xero.sales_account_code'),
        ];
      }

      $result = \XeroLaravel::Invoices($invoice);
      \Log::info(print_r($result,1));

      //todo we are using a discount code (total amount) so need to make a credit note here
      if($sale->discount_code && $sale->discount_code->type == 2) {

      }

      if($sale->voucher_code) {
        $credit_note = [
          [
            'Type' => 'ACCPAYCREDIT',
            'Status' => 'AUTHORISED',
            'Contact' => [
              'ContactID' => $contact_id,
            ],
            'CreditNoteNumber' => $sale->voucher_code->code,
            'LineItemTypes' => 'Inclusive',
            'LineItems' => [
              'LineItem' => [
                'Description' => 'Voucher Code ' . $sale->voucher_code->code,
                'Quantity' => 1,
                'UnitAmount' => $sale->voucher / 1.1, //need the ex-gst value
                'AccountCode' => \Config::get('ecommerce::xero.sales_account_code')
              ]
            ]
          ]
        ];

        $result = \XeroLaravel::CreditNotes($credit_note);
        \Log::info(print_r($result,1));

        $credit_note_id = $result['CreditNotes']['CreditNote']['CreditNoteID'];

        $payment = [
          [
            'Invoice' => [
              'InvoiceID' => $invoice_id
            ],
            'Reference' =>  'Voucher Code ' . $sale->voucher_code->code,
            'Status' => 'AUTHORISED',
            'IsReconciled' => true,
            'Account' => [
              'Code' => \Config::get('ecommerce::xero.prepayments_account_code')
            ],
            'Amount' => $sale->voucher
          ]
        ];

        $result = \XeroLaravel::Payments($payment);
        \Log::info(print_r($result,1));

      }
    }
  }

  private function getResponseDescription($responseCode) {
    switch ($responseCode) {
      case "0" : $result = "Transaction Successful"; break;
      case "?" : $result = "Transaction status is unknown"; break;
      case "1" : $result = "Unknown Error"; break;
      case "2" : $result = "Bank Declined Transaction"; break;
      case "3" : $result = "No Reply from Bank"; break;
      case "4" : $result = "Expired Card"; break;
      case "5" : $result = "Insufficient funds"; break;
      case "6" : $result = "Error Communicating with Bank"; break;
      case "7" : $result = "Payment Server System Error"; break;
      case "8" : $result = "Transaction Type Not Supported"; break;
      case "9" : $result = "Bank declined transaction (Do not contact Bank)"; break;
      case "A" : $result = "Transaction Aborted"; break;
      case "C" : $result = "Transaction Cancelled"; break;
      case "D" : $result = "Deferred transaction has been received and is awaiting processing"; break;
      case "F" : $result = "3D Secure Authentication failed"; break;
      case "I" : $result = "Card Security Code verification failed"; break;
      case "L" : $result = "Shopping Transaction Locked (Please try the transaction again later)"; break;
      case "N" : $result = "Cardholder is not enrolled in Authentication scheme"; break;
      case "P" : $result = "Transaction has been received by the Payment Adaptor and is being processed"; break;
      case "R" : $result = "Transaction was not processed - Reached limit of retry attempts allowed"; break;
      case "S" : $result = "Duplicate SessionID (OrderInfo)"; break;
      case "T" : $result = "Address Verification Failed"; break;
      case "U" : $result = "Card Security Code Failed"; break;
      case "V" : $result = "Address Verification and Card Security Code Failed"; break;
      default  : $result = "Unable to be determined";
    }

    return $result;
  }

  private function null2unknown($map, $key) {
    if(array_key_exists($key, $map)) {
      if(!is_null($map[$key])) {
        return $map[$key];
      }
    }

    return null;
  }

  public function getCommbank()
  {
    $cart = $this->getCart();

    $errors = [];
    foreach($cart->products as $product) {
      if(!$product->virtual) {
        if($product->pivot->options()->colour()->count() && $product->pivot->options()->size()->count()) {
          $colour_id = $product->pivot->options()->colour()->first()->id;
          $size_id = $product->pivot->options()->size()->first()->id;
          $stock = ProductOptionStock::whereRaw('product_id = ? and colour_id = ? and size_id = ?', [$product->id, $colour_id, $size_id])->get();
        } else if($product->pivot->options()->colour()->count()) {
          $colour_id = $product->pivot->options()->colour()->first()->id;
          $stock = ProductOptionStock::whereRaw('product_id = ? and colour_id = ? and size_id is null', [$product->id, $colour_id])->get();
        } else if($product->pivot->options()->size()->count()) {
          $size_id = $product->pivot->options()->size()->first()->id;
          $stock = ProductOptionStock::whereRaw('product_id = ? and size_id = ? and colour_id is null', [$product->id, $size_id])->get();
        } else {
          $stock = ProductOptionStock::whereRaw('product_id = ? and size_id is null and colour_id is null', [$product->id])->get();
        }

        if($stock->count() && ($product->pivot->quantity > $stock->first()->quantity)) {
          $errors[$product->pivot->id] = $stock->first()->quantity;
          \DB::table('product_shopping_cart')->where('id', $product->pivot->id)->update(['quantity' => $stock->first()->quantity]);
        }
      }
    }

    if(count($errors)) {
      \Session::flash('checkout_errors', $errors);
    }

    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'cart' => $cart,
      'customer' => $cart->customer,
      'title' => 'Payment'
    ];

    return \View::make('ecommerce::cart.payment.commbank', $data);

  }

  public function postCommbank()
  {
    if(!\Session::has('cart_id')) {
      return \Redirect::to('cart');
    }

    if(!\Session::has('sale')) {
      return \Redirect::to('/checkout/shipping');
    }

    $data = \Input::all();

    $validator = \Validator::make($data, $this->commbank_rules);

    if($validator->fails()) {
      return json_encode(['status' => 'validation_error']);
    }

    $cart = $this->getCart();

    $postData = "";
    $ampersand = "";

    $data = \Input::all();
    $data['vpc_CardExp'] = $data['vpc_CardExpYear'] . $data['vpc_CardExpMonth'];
    unset($data['vpc_CardExpYear']);
    unset($data['vpc_CardExpMonth']);

    foreach($data as $key => $value) {
      // create the POST data input leaving out any fields that have no value
      if (strlen($value) > 0) {
        $postData .= $ampersand . urlencode($key) . '=' . urlencode($value);
        $ampersand = "&";
      }
    }

    ob_start();

    $ch = curl_init();

    // set the URL of the VPC
    curl_setopt ($ch, CURLOPT_URL, \Config::get('ecommerce::commbank.endpoint'));
    curl_setopt ($ch, CURLOPT_POST, 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_exec ($ch);

    $response = ob_get_contents();
    ob_end_clean();

    $message = "";

    // serach if $response contains html error code
    if(strchr($response,"<html>") || strchr($response,"<html>")) {;
      $message = $response;
    } else {
      // check for errors from curl
      if (curl_error($ch)) {
        $message = "curl_errno=". curl_errno($ch) . "<br/>" . curl_error($ch);
      }
    }

    // close client URL
    curl_close($ch);

    // Extract the available receipt fields from the VPC Response
    // If not present then let the value be equal to 'No Value Returned'
    $map = array();

    // process response if no errors
    if (strlen($message) == 0) {
      $pairArray = explode("&", $response);
      foreach ($pairArray as $pair) {
        $param = explode("=", $pair);
        $map[urldecode($param[0])] = urldecode($param[1]);
      }

      $message = $this->null2unknown($map, "vpc_Message");
    }

    // Standard Receipt Data
    # merchTxnRef not always returned in response if no receipt so get input

    $results = [
      'merchTxnRef'     => \Input::get('vpc_MerchTxnRef'),
      'amount'          => $this->null2unknown($map, "vpc_Amount"),
      'locale'          => $this->null2unknown($map, "vpc_Locale"),
      'batchNo'         => $this->null2unknown($map, "vpc_BatchNo"),
      'command'         => $this->null2unknown($map, "vpc_Command"),
      'version'         => $this->null2unknown($map, "vpc_Version"),
      'cardType'        => $this->null2unknown($map, "vpc_Card"),
      'orderInfo'       => $this->null2unknown($map, "vpc_OrderInfo"),
      'receiptNo'       => $this->null2unknown($map, "vpc_ReceiptNo"),
      'merchantID'      => $this->null2unknown($map, "vpc_Merchant"),
      'authorizeID'     => $this->null2unknown($map, "vpc_AuthorizeId"),
      'transactionNr'   => $this->null2unknown($map, "vpc_TransactionNo"),
      'acqResponseCode' => $this->null2unknown($map, "vpc_AcqResponseCode"),
      'txnResponseCode' => $this->null2unknown($map, "vpc_TxnResponseCode"),

      // CSC Receipt Data
      'cscResultCode'   => $this->null2unknown($map, "vpc_CSCResultCode"),
      'cscACQRespCode'  => $this->null2unknown($map, "vpc_AcqCSCRespCode"),

        // AVS Receipt Data
      'avsResultCode'   => $this->null2unknown($map, "vpc_AVSResultCode"),
      'vACQAVSRespCode' => $this->null2unknown($map, "vpc_AcqAVSRespCode"),
      'avs_City'        => $this->null2unknown($map, "vpc_AVS_City"),
      'avs_Country'     => $this->null2unknown($map, "vpc_AVS_Country"),
      'avs_Street01'    => $this->null2unknown($map, "vpc_AVS_Street01"),
      'avs_PostCode'    => $this->null2unknown($map, "vpc_AVS_PostCode"),
      'avs_StateProv'   => $this->null2unknown($map, "vpc_AVS_StateProv"),
      'avsRequestCode'  => $this->null2unknown($map, "vpc_AVSRequestCode")
    ];

    // Show the display page as an error page
    if ((int)$results['txnResponseCode'] || $results['txnResponseCode'] == 'E') {
      return json_encode(['status' => 'payment_error']);
    } else {
      return $this->processSale($cart, 'commbank', true);
    }

  }

  public function postCod()
  {
    if(!\Session::has('cart_id')) {
      return \Redirect::to('cart');
    }

    $cart = $this->getCart();

    return $this->processSale($cart, 'cod');
  }

  public function getPin()
  {
    if(!\Session::has('cart_id')) {
      return \Redirect::to('cart');
    }

    $cart = $this->getCart();

    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'cart' => $cart,
      'title' => 'Payment',
    ];

    return \View::make('ecommerce::cart.payment.pin', $data);
  }

  public function postPin()
  {
    if(!\Session::has('cart_id')) {
      return \Redirect::to('cart');
    }

    $card_token = \Input::get('card_token');

    $cart = $this->getCart();

    $gateway = Omnipay::create('Pin');
    $gateway->setSecretKey(\Config::get('ecommerce::payment.pin.secret_key'));

    if(\App::environment('local', 'development', 'staging')) {
      $gateway->setTestMode(true);
    }

    $response = $gateway->purchase([
      'email'       => $cart->customer->email,
      'description' => \Config::get('ecommerce::payment.pin.charge.description'),
      'amount'      => $cart->total,
      'currency'    => \Config::get('ecommerce::payment.pin.charge.currency'),
      'token'  => $card_token,
      'card' => [
        'email' => $cart->customer->email
      ],
      'client_ip' => $_SERVER['REMOTE_ADDR']
    ])->send();

    if ($response->isSuccessful()) {

      //If the transaction was successful then create the sale
      return $this->processSale($cart, 'pin');
    } elseif ($response->isRedirect()) {

      //Redirect to offsite payment gateway
      return $response->redirect();
    } else {
      return \Redirect::to('payment/unsuccessful');
    }

  }

  public function getSuccessful()
  {
    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'payment-successful')->first() ?: \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'title' => 'Payment Successful'
    ];

    return \View::make('ecommerce::cart.successful', $data);
  }

  public function getUnsuccessful()
  {
    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'payment-unsuccessful')->first() ?: \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'title' => 'Payment Unsuccessful'
    ];

    return \View::make('ecommerce::cart.unsuccessful', $data);
  }

  public function postDiscount()
  {
    if(!\Session::has('cart_id')) {
      return \Redirect::to('cart');
    }

    $cart = $this->getCart();

    $data = \Input::all();

    $validator = \Validator::make($data, $this->voucher_rules);

    if($validator->fails()) {
      $validator = \Validator::make($data, $this->discount_rules);

      if($validator->fails()) {
        return \Redirect::to('payment/creditcard')->withErrors($validator)->withInput();
      }

      $discount_code = DiscountCode::where('code', '=', $data['code'])->first();

      $cart->discount_code_id = $discount_code->id;
      $cart->save();

    } else {
      $voucher = Voucher::where('code', '=', $data['code'])->first();

      $cart->voucher_id = $voucher->id;
      $cart->save();
    }

    return \Redirect::to('payment/creditcard');
  }

}
