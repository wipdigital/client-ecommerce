<?php namespace WorkInProgress\ClientEcommerce;

class ProductsController extends \BaseController {

  public function __construct()
  {
    $this->beforeFilter('csrf', array('on' => 'post'));
  }

  public function editProduct($id)
  {

    $cart = \App::make('WorkInProgress\ClientEcommerce\CartController')->getCart();
    $product = $cart->products->filter(function($product) use ($id) {
      return $product->pivot->id == $id;
    })->first();

    if($product) {
      $data = [
        'product' => $product,
        'pages' => \WorkInProgress\ClientPages\Page::find(1),
        'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
        'title' => $product->full_title
      ];

      if(\Session::has('customer_id')) {
        $customer = Customer::find(\Session::get('customer_id'));
        $data['customer'] = $customer;
      }

      //Display product
      return \View::make('ecommerce::product', $data);
    }
  }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getProduct($full_permalink = 'products')
  {
    //Look for a category
    $category = Category::whereRaw('full_permalink = ? and active = 1', [$full_permalink])->first();

    if($category) {
      $data = [
        'category' => $category,
        'pages' => \WorkInProgress\ClientPages\Page::find(1),
        'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
        'title' => $category->full_title
      ];

      //Display category
      return \View::make('ecommerce::category', $data);
    }

    //Look for a product instead
    $product = Product::whereRaw('full_permalink = ? and active = 1', [$full_permalink])->first();

    if($product) {
      $data = [
        'product' => $product,
        'pages' => \WorkInProgress\ClientPages\Page::find(1),
        'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
        'title' => $product->full_title
      ];

      if(\Session::has('customer_id')) {
        $customer = Customer::find(\Session::get('customer_id'));
        $data['customer'] = $customer;
      }

      //Display product
      return \View::make('ecommerce::product', $data);
    }

    return \Redirect::to('404');
  }

  public function getSearch()
  {
    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'title' => 'Search',
      'query' => ''
    ];

    return \View::make('ecommerce::search.index', $data);
  }

  public function postSearch()
  {
    $query = \Input::get('query');

    $search_results = Product::search($query)->active()->get();

    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'title' => 'Search Results',
      'query' => $query,
      'search_results' => $search_results
    ];

    return \View::make('ecommerce::search.index', $data);
  }

}

?>
