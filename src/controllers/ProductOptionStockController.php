<?php namespace WorkInProgress\ClientEcommerce;

class ProductOptionStockController extends \BaseController {

  public function __construct()
  {
    //$this->beforeFilter('ajax');
  }

  public function getReserved($product_id)
  {
    $product = Product::findOrFail($product_id);

    return json_encode(['is_reserved' => $product->is_reserved], JSON_NUMERIC_CHECK);
  }

  public function getStockFromProductOption()
  {
    if(\Config::get('ecommerce::product.stock_control')) {
      $full_permalink = \Input::get('full_permalink');
      $product = Product::where('full_permalink', '=', $full_permalink)->firstOrFail();
      $colour_id = \Input::has('colour_id') ? \Input::get('colour_id') : null;
      $size_id = \Input::has('size_id') ? \Input::get('size_id') : null;

      if($product->virtual) {
        return json_encode(['quantity' => 1], JSON_NUMERIC_CHECK);
      } else {
        if($colour_id && $size_id) {
          $stock = ProductOptionStock::whereRaw('product_id = ? and colour_id = ? and size_id = ?', [$product->id, $colour_id, $size_id])->first();
        } else if($colour_id) {
          $stock = ProductOptionStock::whereRaw('product_id = ? and colour_id = ? and size_id is null', [$product->id, $colour_id])->first();
        } else if($size_id) {
          $stock = ProductOptionStock::whereRaw('product_id = ? and size_id = ? and colour_id is null', [$product->id, $size_id])->first();
        } else {
          $stock = ProductOptionStock::whereRaw('product_id = ? and size_id is null and colour_id is null', [$product->id])->first();
        }

        return json_encode(['quantity' => $stock->quantity], JSON_NUMERIC_CHECK);
      }
    }

    return json_encode(['quantity' => 1], JSON_NUMERIC_CHECK);
  }

  public function getStockFromProduct($full_permalink, $quantity = 1)
  {
    if(\Config::get('ecommerce::product.stock_control')) {
      $product = Product::where('full_permalink', '=', $full_permalink)->firstOrFail();
      $stocks = ProductOptionStock::whereRaw('size_id is null and colour_id is null and product_id = ? and quantity >= ? and deleted_at is null', [$product->id, $quantity])->lists('product_id');
    } else {
      $product = Product::where('full_permalink', '=', $full_permalink)->firstOrFail();
      $stocks = ProductOptionStock::whereRaw('size_id is null and colour_id is null and product_id = ? and quantity <= ? and deleted_at is null', [$product->id, $quantity])->lists('product_id');
    }

    return json_encode($stocks, JSON_NUMERIC_CHECK);
  }

  public function getSizeFromColour($full_permalink, $id = null, $quantity = 1)
  {
    if(\Config::get('ecommerce::product.stock_control')) {
      if($id) {
        $stocks = ProductOptionStock::whereRaw('colour_id = ? and quantity >= ? and deleted_at is null', [$id, $quantity])->lists('size_id');
      } else {
        $product = Product::where('full_permalink', '=', $full_permalink)->firstOrFail();
        $stocks = ProductOptionStock::whereRaw('size_id is not null and product_id = ? and quantity >= ? and deleted_at is null', [$product->id, $quantity])->lists('size_id');
      }
    } else {
      if($id) {
        $stocks = ProductOptionStock::whereRaw('colour_id = ? and quantity <= ? and deleted_at is null', [$id, $quantity])->lists('size_id');
      } else {
        $product = Product::where('full_permalink', '=', $full_permalink)->firstOrFail();
        $stocks = ProductOptionStock::whereRaw('size_id is not null and product_id = ? and quantity <= ? and deleted_at is null', [$product->id, $quantity])->lists('size_id');
      }
    }

    return json_encode($stocks, JSON_NUMERIC_CHECK);
  }

  public function getColourFromSize($full_permalink, $id = null, $quantity = 1)
  {
    if(\Config::get('ecommerce::product.stock_control')) {
      if($id) {
        $stocks = ProductOptionStock::whereRaw('size_id = ? and quantity >= ? and deleted_at is null', [$id, $quantity])->lists('colour_id');
      } else {
        $product = Product::where('full_permalink', '=', $full_permalink)->firstOrFail();
        $stocks = ProductOptionStock::whereRaw('colour_id is not null and product_id = ? and quantity >= ? and deleted_at is null', [$product->id, $quantity])->lists('colour_id');
      }
    } else {
      if($id) {
        $stocks = ProductOptionStock::whereRaw('size_id = ? and quantity <= ? and deleted_at is null', [$id, $quantity])->lists('colour_id');
      } else {
        $product = Product::where('full_permalink', '=', $full_permalink)->firstOrFail();
        $stocks = ProductOptionStock::whereRaw('colour_id is not null and product_id = ? and quantity <= ? and deleted_at is null', [$product->id, $quantity])->lists('colour_id');
      }
    }

    return json_encode($stocks, JSON_NUMERIC_CHECK);
  }

}
