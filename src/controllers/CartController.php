<?php namespace WorkInProgress\ClientEcommerce;

use Carbon\Carbon;

class CartController extends \BaseController {

  public function __construct()
  {
    $this->beforeFilter('csrf', ['on' => 'post']);
  }

  public function getCart() {
    if(\Session::has('customer_id') && \Session::has('cart_id')) {
      $customer = Customer::findOrFail(\Session::get('customer_id'));
      $cart = Cart::findOrFail(\Session::get('cart_id'));  

      $customer->shopping_cart_id = $cart->id;
      $customer->save();

      return $cart;
    }

    if(\Session::has('cart_id')) {
      $cart = Cart::findOrFail(\Session::get('cart_id'));  

      return $cart;
    }

    if(\Session::has('customer_id')) {
      $customer = Customer::findOrFail(\Session::get('customer_id'));

      if(isset($customer->cart)) {
        \Session::put('cart_id', $customer->cart->id);

        return $customer->cart;
      }
    }

    $cart = new Cart;
    $cart->save();
    
    \Session::put('cart_id', $cart->id);

    return $cart;
  }

  public function postProduct() {
    $full_permalink = \Input::get('full_permalink');
    $quantity = \Input::get('quantity');

    $product = Product::where('full_permalink', '=', $full_permalink)->firstOrFail();

    if($product) {
      $cart = $this->getCart();

      if($quantity) {
        //There is a quantity so add/update the product/s
        
        if($product->product_set) {
          //todo
          //handle product sets
        } else {
          //There are no product sets so run normally
          if(\Input::has('pivot_id')) {
            //if it has pivot id then we want to only update its options
            $id = \Input::get('pivot_id');
            $product = $cart->products->filter(function($product) use ($id) {
                return $product->pivot->id == $id;
            })->first();

            $quantity = $product->pivot->quantity;
            \DB::table('product_shopping_cart')->where('id', $product->pivot->id)->delete();
            $cart = $this->getCart();
          }
          
          if(\Input::has('colour_id') || \Input::has('size_id') || \Input::has('variation_id')) {
            //Filter the carts current products by their colour and size options
            $products = $cart->products->filter(function($product) {
              if(\Input::has('variation_id')) {
                $return = true;

                if($product->pivot->options()->variation()->count()) {
                  foreach(\Input::get('variation_id') as $variation_id) {
                    if(!$product->pivot->options()->variation()->get()->contains($variation_id)) {
                      $return = false;
                    }
                  }

                  if($return) {
                    return $product;
                  }
                }
              } else if(\Input::has('colour_id') && \Input::has('size_id')) {
                if($product->pivot->options()->colour()->count() && $product->pivot->options()->size()->count() && $product->pivot->options()->colour()->first()->id == \Input::get('colour_id') && $product->pivot->options()->size()->first()->id == \Input::get('size_id')) {
                  return $product;
                }
              } else if(\Input::has('size_id')) {
                if($product->pivot->options()->size()->count() && $product->pivot->options()->size()->first()->id == \Input::get('size_id')) {
                  return $product;
                }
              } else if(\Input::has('colour_id')) {
                if($product->pivot->options()->colour()->count() && $product->pivot->options()->colour()->first()->id == \Input::get('colour_id')) {
                  return $product;
                }
              }
            });

            if(count($products)) {
              //Update existing product variation
              $pivot = $products->first()->pivot;
              if(!\Input::has('update_quantity')) {
                $quantity = $pivot->quantity + $quantity;
              }
              \DB::table('product_shopping_cart')->where('id', $pivot->id)->update(['quantity' => $quantity]);
            } else {
              //Add new product variation
              $cart->products()->attach($product->id, ['quantity' => $quantity]);
              $cart = $this->getCart();
              
              $pivot = $cart->products()->withPivot('id')->orderBy('pivot_id', 'desc')->first()->pivot;

              //Add colour and size options
              $data = [];
              if(\Input::has('colour_id')) {
                $data[] = \Input::get('colour_id');
              }

              if(\Input::has('size_id')) {
                $data[] = \Input::get('size_id');
              }

              if(\Input::has('variation_id')) {
                foreach(\Input::get('variation_id') as $variation_id) {
                  $data[] = $variation_id;
                }
              }

              $pivot->options()->sync($data);
            }
          } else {
            if($cart->products->contains($product->id)) {
              //Update existing product (not variation)
              $pivot = $cart->products->filter(function($p) use ($product) {
                return ( $p->id == $product->id ) && !$p->pivot->options()->count();
              })->first()->pivot;

              //check if product doesn't contain any variations and create a new product if it does
              if($pivot->options()->count()) {
                $cart->products()->attach($product->id, ['quantity' => $quantity]);
              } else {
                if(!\Input::has('update_quantity')) {
                  $quantity = $pivot->quantity + $quantity;
                }

                \DB::table('product_shopping_cart')->where('id', $pivot->id)->update(['quantity' => $quantity]);
              }

            } else {
              //Add new product (not variation)
              $cart->products()->attach($product->id, ['quantity' => $quantity]);

              //todo cheep.com.au specific code
              if($product->parent_category->permalink == 'vintage') {
                $timeout_at = Carbon::now();
                $timeout_at->minute += \Config::get('ecommerce::product.vintage_timeout');
                $product->timeout_at = $timeout_at;
                $product->save();
              }

            }

          }

        }

      } else {
        //There is no quantity so delete
        if(\Input::has('colour_id') || \Input::has('size_id') || \Input::has('variation_id')) {
          $products = $cart->products->filter(function($product) {
            if(\Input::has('variation_id')) {
              $return = true;

              if($product->pivot->options()->variation()->count()) {
                foreach(\Input::get('variation_id') as $variation_id) {
                  if(!$product->pivot->options()->variation()->get()->contains($variation_id)) {
                    $return = false;
                  }
                }

                if($return) {
                  return $product;
                }
              }
            } else if(\Input::has('colour_id') && \Input::has('size_id')) {
              if($product->pivot->options()->colour()->count() && $product->pivot->options()->size()->count() && $product->pivot->options()->colour()->first()->id == \Input::get('colour_id') && $product->pivot->options()->size()->first()->id == \Input::get('size_id')) {
                return $product;
              }
            } else if(\Input::has('size_id')) {
              if($product->pivot->options()->size()->count() && $product->pivot->options()->size()->first()->id == \Input::get('size_id')) {
                return $product;
              }
            } else if(\Input::has('colour_id')) {
              if($product->pivot->options()->colour()->count() && $product->pivot->options()->colour()->first()->id == \Input::get('colour_id')) {
                return $product;
              }
            }
          });

          if(count($products)) {
            //Delete existing product variation
            $pivot = $products->first()->pivot;
            \DB::table('product_shopping_cart')->where('id', $pivot->id)->delete();
          }
        } else {
          //need to check if there are other variations and only return the one with no variations
          $pivot = $cart->products->filter(function($p) use ($product) {
            return ( $p->id == $product->id ) && !$p->pivot->options()->count();
          })->first()->pivot;

          \DB::table('product_shopping_cart')->where('id', $pivot->id)->delete();
        }

      } 

      return \Redirect::to('cart');
    } 

  }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
  {
    $cart = $this->getCart();

    $total = 0.00;
    foreach($cart->products as $product) {
      $total += $product->pivot->quantity * ($product->price + $product->pivot->price_offset);
    }

    $gst = $total / 11;

    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'title' => 'Cart',
      'cart' => $cart,
      'total' => $total,
      'gst' => $gst
    ];

    return \View::make('ecommerce::cart.index', $data);
	}

	public function getLast()
  {
    if(\Session::has('customer_id')) {
      $customer = Customer::findOrFail(\Session::get('customer_id'));
      $cart = $customer->last_cart;

      $data = [
        'pages' => \WorkInProgress\ClientPages\Page::find(1),
        'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
        'title' => 'Last Cart',
        'cart' => $cart
      ];

      return \View::make('ecommerce::cart.last_order', $data);
    }

	}

  public function postLast()
  {
    if(\Session::has('customer_id')) {
      //get all products in last cart and merge with new cart - redirect to summary/billing page
      $customer = Customer::findOrFail(\Session::get('customer_id'));
      $cart = $this->getCart();

      //clear current cart
      $cart->products()->sync([]);

      //go through each product in last cart and recreate on cart
      foreach($customer->last_cart->products as $last_product) {
        //Add new product variation
        $cart->products()->attach($last_product->id, ['quantity' => $last_product->pivot->quantity]);
        $cart = $this->getCart();
        
        $pivot = $cart->products()->withPivot('id')->orderBy('pivot_id', 'desc')->first()->pivot;

        //Add colour and size options
        $data = [];
        foreach($last_product->options as $option) {
          $data[] = $option->id;
        }

        $pivot->options()->sync($data);
      }

      $redirect_uri = \Input::has('redirect_uri') ? \Input::get('redirect_uri') : '/checkout/summary';
      return \Redirect::to($redirect_uri);
    }
  }

  public function getTimeout()
  {
    $cart = $this->getCart();

    $timeout = 0;
    $now = Carbon::now();
    $cart->products->each(function($product) use (&$timeout, $now) {
      if($product->timeout_at) {
        if($product->timeout_at->gte($now)) {
          $timeout = Carbon::now()->diffInSeconds($product->timeout_at);
        } else {
          \DB::table('product_shopping_cart')->where('product_id', $product->id)->delete();
        }
      }
    });

    $json = [
      'timeout' => $timeout
    ];

    return json_encode($json);
  }

}
