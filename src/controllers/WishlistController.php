<?php namespace WorkInProgress\ClientEcommerce;

use Carbon\Carbon;

class WishlistController extends \BaseController {

  public function __construct()
  {
    $this->beforeFilter('csrf', ['on' => 'post']);
  }

  public function getWishlist() {
    if(\Session::has('customer_id')) {
      $customer = Customer::findOrFail(\Session::get('customer_id'));

      if($customer->wishlist) {

        return $customer->wishlist;
      }

      $wishlist = new Wishlist;
      $wishlist->customer_id = $customer->id;
      $wishlist->save();

      return $wishlist;
    }

    return false; 
  }

  public function postCart() {
    $full_permalink = \Input::get('full_permalink');
    $quantity = \Input::get('quantity');

    $product = Product::where('full_permalink', '=', $full_permalink)->firstOrFail();
    $wishlist = $this->getWishlist();

    if($product) {
      $cart = \App::make('WorkInProgress\ClientEcommerce\CartController')->getCart();

      if($product->product_set) {
        //todo add product set to wishlist
      } else {
        //There are no product sets so run normally
        if(\Input::has('colour_id') || \Input::has('size_id')) {
          //Filter the carts current products by their colour and size options
          $products = $cart->products->filter(function($product) {
            if(\Input::has('colour_id') && \Input::has('size_id')) {
              if($product->pivot->options()->colour()->count() && $product->pivot->options()->size()->count() && $product->pivot->options()->colour()->first()->id == \Input::get('colour_id') && $product->pivot->options()->size()->first()->id == \Input::get('size_id')) {
                return $product;
              }
            } else if(\Input::has('size_id')) {
              if($product->pivot->options()->size()->count() && $product->pivot->options()->size()->first()->id == \Input::get('size_id')) {
                return $product;
              }
            } else if(\Input::has('colour_id')) {
              if($product->pivot->options()->colour()->count() && $product->pivot->options()->colour()->first()->id == \Input::get('colour_id')) {
                return $product;
              }
            }
          });

          if(!count($products)) {
            //Add new product variation
            $cart->products()->attach($product->id, ['quantity' => $quantity]);
            $cart = \App::make('WorkInProgress\ClientEcommerce\CartController')->getCart();
            
            $pivot = $cart->products()->withPivot('id')->orderBy('pivot_id', 'desc')->first()->pivot;

            //Add colour and size options
            $data = [];
            if(\Input::has('colour_id')) {
              $data[] = \Input::get('colour_id');
            }

            if(\Input::has('size_id')) {
              $data[] = \Input::get('size_id');
            }

            $pivot->options()->sync($data);
          }

          $products = $wishlist->products->filter(function($product) {
            if(\Input::has('colour_id') && \Input::has('size_id')) {
              if($product->pivot->options()->colour()->count() && $product->pivot->options()->size()->count() && $product->pivot->options()->colour()->first()->id == \Input::get('colour_id') && $product->pivot->options()->size()->first()->id == \Input::get('size_id')) {
                return $product;
              }
            } else if(\Input::has('size_id')) {
              if($product->pivot->options()->size()->count() && $product->pivot->options()->size()->first()->id == \Input::get('size_id')) {
                return $product;
              }
            } else if(\Input::has('colour_id')) {
              if($product->pivot->options()->colour()->count() && $product->pivot->options()->colour()->first()->id == \Input::get('colour_id')) {
                return $product;
              }
            }
          });

          if(count($products)) {
            $pivot = $products->first()->pivot;

            \DB::table('product_wishlist')->where('id', $pivot->id)->delete();
          }
        } else {
          if(!$cart->products->contains($product->id)) {
            //Add new product (not variation)
            $cart->products()->attach($product->id, ['quantity' => $quantity]);

            if($product->parent_category->permalink == 'vintage') {
              $timeout_at = Carbon::now();
              $timeout_at->minute += \Config::get('ecommerce::product.vintage_timeout');
              $product->timeout_at = $timeout_at;
              $product->save();
            }
          }

          $wishlist->products()->detach($product->id);
        }
      }

      return \Redirect::to('cart');
    } 

  }

  public function postProduct() {
    $full_permalink = \Input::get('full_permalink');
    $quantity = \Input::get('quantity');

    $product = Product::where('full_permalink', '=', $full_permalink)->firstOrFail();

    if($product) {
      $wishlist = $this->getWishlist();

      if(!$wishlist) {
        return \Redirect::to('products');
      }

      if($quantity) {
        if($product->product_set) {
          //todo add product set to wishlist
        } else {
          //There are no product sets so run normally
          if(\Input::has('colour_id') || \Input::has('size_id')) {
            //Filter the carts current products by their colour and size options
            $products = $wishlist->products->filter(function($product) {
              if(\Input::has('colour_id') && \Input::has('size_id')) {
                if($product->pivot->options()->colour()->count() && $product->pivot->options()->size()->count() && $product->pivot->options()->colour()->first()->id == \Input::get('colour_id') && $product->pivot->options()->size()->first()->id == \Input::get('size_id')) {
                  return $product;
                }
              } else if(\Input::has('size_id')) {
                if($product->pivot->options()->size()->count() && $product->pivot->options()->size()->first()->id == \Input::get('size_id')) {
                  return $product;
                }
              } else if(\Input::has('colour_id')) {
                if($product->pivot->options()->colour()->count() && $product->pivot->options()->colour()->first()->id == \Input::get('colour_id')) {
                  return $product;
                }
              }
            });

            if(!count($products)) {
              //Add new product variation
              $wishlist->products()->attach($product->id);
              $wishlist = $this->getWishlist();
              
              $pivot = $wishlist->products()->withPivot('id')->orderBy('pivot_id', 'desc')->first()->pivot;

              //Add colour and size options
              $data = [];
              if(\Input::has('colour_id')) {
                $data[] = \Input::get('colour_id');
              }

              if(\Input::has('size_id')) {
                $data[] = \Input::get('size_id');
              }

              $pivot->options()->sync($data);
            }
          } else {
            if(!$wishlist->products->contains($product->id)) {
              //Add new product (not variation)
              $wishlist->products()->attach($product->id);
            }
          }
        }
      } else {
        if(\Input::has('colour_id') || \Input::has('size_id')) {
          $products = $wishlist->products->filter(function($product) {
            if(\Input::has('colour_id') && \Input::has('size_id')) {
              if($product->pivot->options()->colour()->count() && $product->pivot->options()->size()->count() && $product->pivot->options()->colour()->first()->id == \Input::get('colour_id') && $product->pivot->options()->size()->first()->id == \Input::get('size_id')) {
                return $product;
              }
            } else if(\Input::has('size_id')) {
              if($product->pivot->options()->size()->count() && $product->pivot->options()->size()->first()->id == \Input::get('size_id')) {
                return $product;
              }
            } else if(\Input::has('colour_id')) {
              if($product->pivot->options()->colour()->count() && $product->pivot->options()->colour()->first()->id == \Input::get('colour_id')) {
                return $product;
              }
            }
          });

          if(count($products)) {
            //Delete existing product variation
            $pivot = $products->first()->pivot;
            \DB::table('product_wishlist')->where('id', $pivot->id)->delete();
          }
        } else {
          //Delete existing product (not variation)
          $wishlist->products()->detach($product->id);
        }
      }

      return \Redirect::to('wishlist');
    } 

  }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
  {
    $wishlist = $this->getWishlist();

    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'title' => 'Wishlist',
      'wishlist' => $wishlist,
    ];

    return \View::make('ecommerce::wishlist.index', $data);
	}

  /*public function getShare($id)
  {
    $wishlist = Wishlist::findOrFail($id);

    $data = [
      'pages' => \WorkInProgress\ClientPages\Page::find(1),
      'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'products')->first(),
      'title' => 'Wishlist',
      'wishlist' => $wishlist,
    ];

    return \View::make('ecommerce::wishlist.share', $data);
  }*/

}
