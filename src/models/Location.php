<?php namespace WorkInProgress\ClientEcommerce;

class Location extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'locations';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = ['id'];

  protected $fillable = ['postcode', 'suburb', 'state', 'price'];

}
