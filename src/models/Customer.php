<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Customer extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customers';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['last_shopping_cart_id', 'shopping_cart_id', 'facebook_id', 'instagram_id', 'name', 'email', 'address1', 'address2', 'city', 'postcode', 'state', 'country', 'profile_picture', 'suburb', 'phone'];

  public function cart()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Cart', 'shopping_cart_id');
  }

  public function lastCart()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Cart', 'last_shopping_cart_id');
  }

  public function wishlist()
  {
    return $this->hasOne('\WorkInProgress\ClientEcommerce\Wishlist');
  }

  public function sales()
  {
    return $this->hasMany('\WorkInProgress\ClientEcommerce\Sale');
  }
}

?>
