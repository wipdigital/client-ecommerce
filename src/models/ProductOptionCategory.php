<?php namespace WorkInProgress\ClientEcommerce;

class ProductOptionCategory extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_option_categories';

  protected $dates = ['created_at', 'updated_at'];

  protected $fillable = ['title'];

  protected $guarded = ['id'];

  public function options()
  {
    return $this->hasMany('\WorkInProgress\ClientEcommerce\ProductOption', 'product_option_id');
  }

}

?>
