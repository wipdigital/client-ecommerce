<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ProductWishlist extends Pivot {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_wishlist';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = ['id'];

  protected $fillable = ['wishlist_id', 'product_id'];

  protected $appends = ['price_offset'];

  public function options()
  {
    return $this->belongsToMany('\WorkInProgress\ClientEcommerce\ProductOption', 'product_option_product_wishlist', 'product_wishlist_id', 'product_option_id');
  }

  public function getPriceOffsetAttribute($value)
  {
    $offset = 0;
    if($this->options()->colour()->count()) {
      $offset += $this->options()->colour()->first()->price;
    }

    if($this->options()->size()->count()) {
      $offset += $this->options()->size()->first()->price;
    }

    return $offset;
  }

}

?>
