<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;

class Product extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';

  protected $dates = ['created_at', 'updated_at', 'deleted_at', 'timeout_at'];

  protected $guarded = ['id'];

  protected $fillable = ['product_category_id', 'title', 'seo_title', 'permalink', 'full_permalink', 'short_description', 'description', 'price', 'featured', 'active', 'order', 'product_set', 'virtual'];

  public function newPivot(\Eloquent $parent, array $attributes, $table, $exists) 
  {
    switch($table) {
      case 'product_shopping_cart':
        return new ProductShoppingCart($parent, $attributes, $table, $exists);
        break;

      case 'product_wishlist':
      default:
        return new ProductWishlist($parent, $attributes, $table, $exists);
        break;
    }
  }

  public function options()
  {
    return $this->hasMany('\WorkInProgress\ClientEcommerce\ProductOption');
  }

  public function parentCategory()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Category', 'product_category_id');
  }

  public function tags()
  {
    return $this->belongsToMany('\WorkInProgress\ClientEcommerce\Tag');
  }

  public function related()
  {
    return $this->belongsToMany('\WorkInProgress\ClientEcommerce\Product', 'product_product', 'product_id', 'related_product_id');
  }

  public function sets()
  {
    return $this->belongsToMany('\WorkInProgress\ClientEcommerce\Product', 'product_sets', 'product_id', 'product_set_id');
  }

  public function images()
  {
    return $this->hasMany('\WorkInProgress\ClientEcommerce\ProductImage');
  }

  public function getThumbnailImageAttribute()
  {
    return $this->images->count() ? $this->images->get(0) : null;
  }

  public function getBreadcrumbsAttribute()
  {
    $collection = new Collection;

    for($category = $this->parent_category; isset($category->id); $category = $category->parent_category)
    {
      $collection->add($category);
    }

    return $collection->reverse();
  }

  public function getIsSoldAttribute()
  {
    $product = Product::find($this->attributes['id']);

    //check to see if there is any options
    if(!$product->options()->size()->count() && !$product->options()->colour()->count()) {

      $stock_query = ProductOptionStock::whereRaw('size_id is null and colour_id is null and product_id = ?', [$product->id]);

      if($stock_query->count() && $stock_query->first()->quantity > 0) {
        return false;
      }

      return true;
    }

    return false;
  }
  public function getIsReservedAttribute()
  {
    $product = Product::find($this->attributes['id']);

    if(isset($product->timeout_at) && !$this->is_sold) {
      $now = Carbon::now();
      if($now->gte($product->timeout_at)) {
        $product->timeout_at = null;
        $product->save();
        \DB::table('product_shopping_cart')->where('product_id', $product->id)->delete();

        return false;
      }

      return true;
    }

    return false;
  }

  public function scopeActive($query)
  {
    return $query->where('active', '=', true)->orderBy('order');
  }

  public function scopeSearch($query,$q) {
    return empty($q) ? $query : $query->whereRaw(
    "MATCH(title,short_description,description)
      AGAINST(? IN BOOLEAN MODE)",[$q]);
  }

  public function getFullTitleAttribute()
  {
    return $this->attributes['seo_title'] ?: $this->attributes['title'];
  }

}

?>
