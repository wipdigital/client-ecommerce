<?php namespace WorkInProgress\ClientEcommerce;

class ProductImage extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_images';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = array('id');

  public function parentProduct()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Product', 'product_id');
  }

}

?>
