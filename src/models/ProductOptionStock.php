<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ProductOptionStock extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_option_stock';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['product_id', 'colour_id', 'size_id', 'sku', 'quantity'];

  //todo this will need looking at
  public function product()
  {
    return $this->belongsTo('\WorkInProgress\Ecommerce\ProductOption');
  }

  //todo shouldn't this be a hasOne relationship?
  public function colour()
  {
    return $this->belongsTo('\WorkInProgress\Ecommerce\ProductOption', 'colour_id');
  }

  public function size()
  {
    return $this->belongsTo('\WorkInProgress\Ecommerce\ProductOption', 'size_id');
  }
  
  public function getFullTitleAttribute($value)
  {
    $title = $this->product->title;

    if(isset($this->attributes['colour_id']) || isset($stock->attributes['size_id'])) {
      if(isset($this->attributes['colour_id'])) {
        $title .=  ' ' . $this->colour->title;
      }

      if(isset($this->attributes['size_id)'])) {
        $title .= ' ' . $this->size->title;
      }
    }

    return $title;
  }

}

?>
