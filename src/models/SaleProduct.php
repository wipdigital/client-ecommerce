<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SaleProduct extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sale_products';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['sale_id', 'title', 'short_description', 'price', 'quantity', 'thumbnail', 'product_id', 'product_option_stock_id'];

  public function stock()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\ProductOptionStock', 'product_option_stock_id');
  }

  public function sale()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Sale');
  }

  public function options()
  {
    return $this->belongsToMany('\WorkInProgress\ClientEcommerce\ProductOption');
  }

  public function product()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Product');
  }

}

?>

