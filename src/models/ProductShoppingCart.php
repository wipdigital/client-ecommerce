<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ProductShoppingCart extends Pivot {

  //use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_shopping_cart';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = ['id'];

  protected $fillable = ['shopping_cart_id', 'product_id', 'quantity'];

  protected $appends = ['price_offset'];

  public function options()
  {
    return $this->belongsToMany('\WorkInProgress\ClientEcommerce\ProductOption', 'product_option_product_shopping_cart', 'product_shopping_cart_id', 'product_option_id');
  }

  public function getPriceOffsetAttribute($value)
  {
    $offset = 0;
    if($this->options()->count()) {
      foreach($this->options()->get() as $option) {
        $offset += $option->price;
      }
    }

    return $offset;
  }

}

?>
