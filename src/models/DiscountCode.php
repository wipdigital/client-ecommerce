<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class DiscountCode extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'discount_codes';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  public function scopeActive($query)
  {
    return $query->where('active', '=', true);
  }

}

?>
