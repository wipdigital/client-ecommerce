<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\Collection;

class Tag extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tags';

  protected $dates = ['created_at', 'updated_at'];

  protected $fillable = ['value'];

  protected $guarded = ['id'];

  public function products()
  {
    return $this->belongsToMany('\WorkInProgress\ClientEcommerce\Product');
  }

  //@TODO should really rename value to title in the database
  public function getTitleAttribute()
  {
    return $this->attributes['value'];
  }

  public function getFullPermalinkAttribute()
  {
    return $this->attributes['value'];
  }

  public function getBreadcrumbsAttribute()
  {
    $collection = new Collection;

    $category = Category::find(1);
    $collection->add($category);


    return $collection->reverse();
  }

}

?>
