<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\Database\Eloquent\Collection;

class Category extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_categories';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = array('id');

  public function products()
  {
    return $this->hasMany('\WorkInProgress\ClientEcommerce\Product', 'product_category_id');
  }

  public function categories()
  {
    return $this->hasMany('\WorkInProgress\ClientEcommerce\Category', 'product_category_id');
  }

  public function parentCategory()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Category', 'product_category_id');
  }

  public function images()
  {
    return $this->hasMany('\WorkInProgress\ClientEcommerce\CategoryImage', 'product_category_id');
  }

  public function getBreadcrumbsAttribute()
  {
    $collection = new Collection;

    for($category = $this->parent_category; isset($category->id); $category = $category->parent_category)
    {
      $collection->add($category);
    }

    return $collection->reverse();
  }

  public function scopeActive($query)
  {
    return $query->where('active', '=', true)->orderBy('order');
  }

  public function getFullTitleAttribute()
  {
    return $this->attributes['seo_title'] ?: $this->attributes['title'];
  }

  public function getThumbnailImageAttribute()
  {
    return $this->images->count() ? $this->images->get(0) : null;
  }
}

?>
