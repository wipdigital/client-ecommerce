<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Voucher extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'vouchers';

  protected $dates = ['created_at', 'updated_at', 'deleted_at', 'start_at', 'end_at'];

  protected $guarded = ['id'];

  protected $fillable = ['title', 'short_description', 'code', 'amount', 'active'];

}

?>
