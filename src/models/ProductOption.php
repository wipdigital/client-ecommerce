<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ProductOption extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_options';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = ['id'];

  protected $fillable = ['product_id', 'product_option_category_id', 'title', 'price', 'src', 'thumbnail_src', 'src_2', 'active'];

  public function product()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Product');
  }

  public function category()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\ProductOptionCategory', 'product_option_category_id');
  }

  public function scopeColour($query)
  {
    $product_option_category = ProductOptionCategory::where('title', 'colour')->firstOrFail();

    return $query->where('product_option_category_id', $product_option_category->id);
  }

  public function scopeSize($query)
  {
    $product_option_category = ProductOptionCategory::where('title', 'size')->firstOrFail();

    return $query->where('product_option_category_id', $product_option_category->id);
  }

  public function scopeVariation($query)
  {
    $product_option_category = ProductOptionCategory::where('title', 'variation')->firstOrFail();

    return $query->where('product_option_category_id', $product_option_category->id);
  }

  public function scopeActive($query)
  {
    return $query->where('active', '=', true);
  }

}

?>
