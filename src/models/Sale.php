<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Carbon\Carbon;

class Sale extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sales';

  protected $dates = ['created_at', 'updated_at', 'deleted_at', 'shipped_at'];

  protected $guarded = ['id'];

  protected $fillable = ['discount_code_id', 'voucher_id', 'customer_id', 'total', 'shipped_at', 'invoice_id', 'shipping_ref', 'discount', 'voucher', 'shipping', 'name', 'phone', 'address1', 'address2', 'city', 'suburb', 'state', 'country', 'postcode', 'email', 'payment_method', 'delivery_method'];

  public function voucherCode()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Voucher', 'voucher_id');
  }

  public function discountCode()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\DiscountCode');
  }

  public function customer()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Customer');
  }

  public function products()
  {
    return $this->hasMany('\WorkInProgress\ClientEcommerce\SaleProduct');
  }
}

?>

