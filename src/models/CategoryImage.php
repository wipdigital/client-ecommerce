<?php namespace WorkInProgress\ClientEcommerce;

class CategoryImage extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_category_images';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = array('id');

  public function parentCategory()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Category', 'product_category_id');
  }

}

?>
