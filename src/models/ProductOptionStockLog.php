<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ProductOptionStockLog extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_option_stock_log';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = ['id'];

  protected $fillable = ['product_id', 'product_option_stock_id', 'type', 'quantity', 'cost'];

}

?>
