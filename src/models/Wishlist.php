<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Wishlist extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'wishlists';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['customer_id'];

  public function customer()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Customer');
  }

  public function products()
  {
    return $this->belongsToMany('\WorkInProgress\ClientEcommerce\Product', 'product_wishlist', 'wishlist_id')->withPivot('id');
  }

}

?>
