<?php namespace WorkInProgress\ClientEcommerce;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Cart extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'shopping_carts';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['discount_code_id', 'voucher_id', 'state'];

  public function voucher()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\Voucher');
  }

  public function discountCode()
  {
    return $this->belongsTo('\WorkInProgress\ClientEcommerce\DiscountCode');
  }

  public function customer()
  {
    //We return the latest record because there could be more than 1 customer with this shopping_cart_id
    return $this->hasOne('\WorkInProgress\ClientEcommerce\Customer', 'shopping_cart_id')->latest();
  }

  public function products()
  {
    return $this->belongsToMany('\WorkInProgress\ClientEcommerce\Product', 'product_shopping_cart', 'shopping_cart_id')->withPivot('quantity', 'id');
  }

  public function getSubTotalAttribute()
  {
    $cart = Cart::find($this->attributes['id']);
    $sub_total = 0.00;

    foreach($cart->products as $product) {
      $sub_total += $product->pivot->quantity * ($product->price + $product->pivot->price_offset);
    }

    return $sub_total;
  }

  public function getTotalAttribute()
  {
    $cart = Cart::find($this->attributes['id']);

    return $cart->sub_total - $cart->total_voucher - $cart->total_discount + $cart->total_shipping;
  }

  public function getSubGstAttribute()
  {
    $cart = Cart::find($this->attributes['id']);

    return $cart->sub_total / 11;
  }

  public function getGstAttribute()
  {
    $cart = Cart::find($this->attributes['id']);

    return $cart->total / 11;
  }

  public function getTotalShippingAttribute()
  {
    $cart = Cart::find($this->attributes['id']);

    //If all the products are virtual then free shipping
    $products = $cart->products->filter(function($product) {
      return !$product->virtual;
    });


    if(!$products->count()) {
      return 0;
    }

    if(\Config::get('ecommerce::cart.shipping.suburb_limit')) {

      if(isset(\Session::get('sale')['postcode'])) {
        return \DB::table('locations')->where('postcode', \Session::get('sale')['postcode'])->first()->price;
      }
    }

    if(\Config::get('ecommerce::cart.shipping.total_limit')) {
      $total = $this->sub_total;
      if($cart->discount_code) {
        switch($cart->discount_code->type) {
          case 1: // '% of TOTAL'
          case 2: // 'SUM of TOTAL'
            $total = $this->sub_total - $this->total_discount;
          break;

          case 3: // '% of SHIP TOTAL'
          case 4:
            $total = $this->sub_total;
          break;
        }
      }

      //go through locations and see if they a price
      if($total < \Config::get('ecommerce::shipping.total_limit')) {
        return \Config::get('ecommerce::shipping.fixed_cost');
     
      }
    }

    return 0;
  }

  public function getTotalVoucherAttribute()
  {
    $cart = Cart::find($this->attributes['id']);

    if($cart->voucher) {
      $total = $cart->sub_total - $cart->total_discount + $cart->total_shipping;
      if($cart->voucher->amount > $total) {
        return $total;
      } else {
        return $cart->voucher->amount;
      }
    }
    
    return 0;
  }

  public function getTotalDiscountAttribute()
  {
    $cart = Cart::find($this->attributes['id']);
    $total_discount = 0.00;

    //todo disabled 3-for-$20 temporarily
    //check to see if the 3-for-$20 discount applies on cheep
    $tag_query = Tag::where('value', '3-for-$20');

    if($tag_query->count()) {
      $tag_id = $tag_query->first()->id;

      $i = 0;
      $cart->products->each(function($product) use (&$i, $tag_id) {
        if($product->tags->contains($tag_id)) {
          $i += $product->pivot->quantity;
        }
      });

      if($i >= 3) {
        $total_discount += floor($i / 3) * 10;
      }
    }

    if($cart->discount_code) {
      switch($cart->discount_code->type) {
        case 1: // '% of TOTAL'
          $total_discount += $cart->discount_code->amount / 100 * $cart->sub_total;
          break;

        case 2: // 'SUM of TOTAL'
          $total_discount += $cart->discount_code->amount;
          break;

        case 3: // '% of SHIP TOTAL'
          $total_discount += $cart->discount_code->amount / 100 * $cart->total_shipping;
          break;

        case 4: //TAG DISCOUNT
          //loop through all products and minus half
          $cart->products->each(function($product) use (&$total_discount) {
            if($product->tags->contains(4)) {
              $total_discount += $product->price / 2 * $product->pivot->quantity;
            }
          });

          break;
      }

    }

    return $total_discount;
  }

}

?>
