@extends('ecommerce::layouts.standard')

@section('main')
    @include('ecommerce::components.navigation.breadcrumbs', ['resource' => $product])

  <div class="ecommerce__product"> 
    {{ Form::open(['url' => 'cart/product', 'class' => 'product-form']) }}
      @if(isset($product->pivot))
        {{ Form::hidden('pivot_id', $product->pivot->id) }}
      @endif
      <div class="row small-collapse medium-uncollapse">
        <div class="small-12 medium-6 columns"> 
          @if($product->images()->first())
          <div class="featured thumbnail">
            <a href="#" class="fa-stack fa-lg back" title="Back"><i class="fa fa-stop fa-stack-2x fa-inverse op"></i><i class="fa fa-angle-left fa-stack-1x fx"></i></a> 
            <a href="#" class="fa-stack fa-lg wish wishlist @if(isset($customer) && count($customer->wishlist) && $customer->wishlist->products->contains($product->id)) wishlist-active @endif" title="Add this product to your wishlist"><i class="fa fa-stop fa-stack-2x fa-inverse op"></i><i class="fa fa-heart fa-stack-1x fx"></i></a> 
            <div class="show-for-large-up interchange-slider">
              <div class="cycle-slideshow" data-cycle-auto-height="1:1" data-cycle-fx="fadeout" data-cycle-timeout=0 data-cycle-pause-on-hover="true" data-cycle-speed=400 data-cycle-pager=".show-for-large-up > .adv-custom-pager" data-cycle-slides="> .cycle-slide" data-cycle-pager-template="">
                @foreach($product->images as $image)
                <div class="cycle-slide">
                    <img src="{{ Config::get('ecommerce::product.cdn') . $image->src . Config::get('ecommerce::product.query') }}" alt="{{ $product->full_title }}" data-magnify-src="{{ Config::get('ecommerce::product.cdn') . $image->src . Config::get('ecommerce::product.zoom_query') }}" class="pp-thumb animated zoomIn">
                </div>
                @endforeach
                @foreach($product->options()->colour()->get() as $option)
                  @if($option->thumbnail_src)
                  <div class="cycle-slide">
                      <img src="{{ Config::get('ecommerce::product.cdn') . $option->thumbnail_src . Config::get('ecommerce::product.query') }}" alt="{{ $option->title }}" data-magnify-src="{{ Config::get('ecommerce::product.cdn') . $option->thumbnail_src . Config::get('ecommerce::product.zoom_query') }}" class="pp-thumb animated zoomIn">
                  </div>
                  @endif
                  @if($option->src)
                  <div class="cycle-slide">
                      <img src="{{ Config::get('ecommerce::product.cdn') . $option->src . Config::get('ecommerce::product.query') }}" alt="{{ $option->title }}" data-magnify-src="{{ Config::get('ecommerce::product.cdn') . $option->src . Config::get('ecommerce::product.zoom_query') }}" class="pp-thumb animated zoomIn">
                  </div>
                  @endif
                  @if($option->src_2)
                  <div class="cycle-slide">
                      <img src="{{ Config::get('ecommerce::product.cdn') . $option->src_2 . Config::get('ecommerce::product.query') }}" alt="{{ $option->title }}" data-magnify-src="{{ Config::get('ecommerce::product.cdn') . $option->src_2 . Config::get('ecommerce::product.zoom_query') }}" class="pp-thumb animated zoomIn">
                  </div>
                  @endif
                @endforeach
              </div>
              <!-- empty element for pager links -->
              <div class="adv-custom-pager">
                @foreach($product->images as $image)
                  <img src="{{ Config::get('ecommerce::product.cdn') . $image->src . Config::get('ecommerce::product.thumbnail_query') }}" alt="{{ $product->full_title }}" class="animated zoomIn" width="40" height="40">
                @endforeach
                @foreach($product->options()->colour()->get() as $option)
                  @if($option->thumbnail_src)
                  <img src="{{ Config::get('ecommerce::product.cdn') . $option->thumbnail_src . Config::get('ecommerce::product.thumbnail_query') }}" data-id="{{ $option->id }}" alt="{{ $option->title }}" class="animated zoomIn" width="40" height="40">
                  @endif
                  @if($option->src)
                  <img src="{{ Config::get('ecommerce::product.cdn') . $option->src . Config::get('ecommerce::product.thumbnail_query') }}" alt="{{ $option->title }}" class="animated zoomIn" width="40" height="40">
                  @endif
                  @if($option->src_2)
                  <img src="{{ Config::get('ecommerce::product.cdn') . $option->src_2 . Config::get('ecommerce::product.thumbnail_query') }}" alt="{{ $option->title }}" class="animated zoomIn" width="40" height="40">
                  @endif
                @endforeach
              </div>
            </div>
            <div class="hide-for-large-up interchange-slider">
              <div class="cycle-slideshow" data-cycle-auto-height="1:1" data-cycle-swipe=true data-cycle-fx="scrollHorz" data-cycle-timeout=0 data-cycle-swipe-fx="scrollHorz" data-cycle-speed=400 data-cycle-pager=".hide-for-large-up .adv-custom-pager" data-cycle-slides="> .cycle-slide" data-cycle-pager-template="">
                @foreach($product->images as $image)
                <div class="cycle-slide">
                  <img src="{{ Config::get('ecommerce::product.cdn') . $image->src . Config::get('ecommerce::product.query') }}" alt="{{ $image->title }}" class="pp-thumb animated zoomIn">
                </div>
                @endforeach
                @foreach($product->options()->colour()->get() as $option)
                  @if($option->thumbnail_src)
                  <div class="cycle-slide">
                    <img src="{{ Config::get('ecommerce::product.cdn') . $option->thumbnail_src . Config::get('ecommerce::product.query') }}" alt="{{ $option->title }}" class="pp-thumb animated zoomIn">
                  </div>
                  @endif
                  @if($option->src)
                  <div class="cycle-slide">
                    <img src="{{ Config::get('ecommerce::product.cdn') . $option->src . Config::get('ecommerce::product.query') }}" alt="{{ $option->title }}" class="pp-thumb animated zoomIn">
                  </div>
                  @endif
                  @if($option->src_2)
                  <div class="cycle-slide">
                    <img src="{{ Config::get('ecommerce::product.cdn') . $option->src_2 . Config::get('ecommerce::product.query') }}" alt="{{ $option->title }}" class="pp-thumb animated zoomIn">
                  </div>
                  @endif
                @endforeach
              </div>
              <!-- empty element for pager links -->
              <div class="adv-custom-pager">
                @foreach($product->images as $image)
                  <img src="{{ Config::get('ecommerce::product.cdn') . $image->src . Config::get('ecommerce::product.thumbnail_query') }}" alt="{{ $product->full_title }}" class="animated zoomIn" width="40" height="40">
                @endforeach
                @foreach($product->options()->colour()->get() as $option)
                  @if($option->thumbnail_src)
                  <img src="{{ Config::get('ecommerce::product.cdn') . $option->thumbnail_src . Config::get('ecommerce::product.thumbnail_query') }}" data-id="{{ $option->id }}" alt="{{ $option->title }}" class="animated zoomIn" width="40" height="40">
                  @endif
                  @if($option->src)
                  <img src="{{ Config::get('ecommerce::product.cdn') . $option->src . Config::get('ecommerce::product.thumbnail_query') }}" alt="{{ $option->title }}" class="animated zoomIn" width="40" height="40">
                  @endif
                  @if($option->src_2)
                  <img src="{{ Config::get('ecommerce::product.cdn') . $option->src_2 . Config::get('ecommerce::product.thumbnail_query') }}" alt="{{ $option->title }}" class="animated zoomIn" width="40" height="40">
                  @endif
                @endforeach
              </div>
            </div>

            @if($product->tags()->count())
            <div class="overlay">
              @foreach($product->tags as $tag)
              <a href="/products/tags/{{ $tag->value }}" class="label">#{{ $tag->value }}</a>
              @endforeach
            </div>
            @endif
          </div>
          @endif
          <div class="show-for-medium-up gap">
            @if($product->related()->active()->count())
            <p class="titles text-center">RELATED PRODUCTS</p>
            <div class="related">
              <div class="related-imgs">
                @foreach($product->related()->active()->get() as $related_product)
                <a href="/products/{{$related_product->full_permalink}}">
                  <img src="{{ Config::get('ecommerce::product.cdn') . $related_product->images()->first()->src . Config::get('ecommerce::product.query') }}" alt="{{ $related_product->full_title }}" class="wow zoomIn" width="170" height="170">
                </a>
                @endforeach
              </div>
            </div><!--end related-->
            @endif
          </div> <!--end Medium-Up-->
        </div><!--End Cols-->

        <div class="small-12 medium-6 columns">
          <div class="title">
            <div class="right price">${{ number_format($product->price, 2) }}</div>
            <h1 class="left">{{ $product->full_title }}</h1>
            <div class="clear"></div>
          </div>
          <div class="description">
            {{ $product->description }}
            <div class="social-shares">
              <div class="fb-share-button" data-href="{{ Request::url() }}" data-layout="button"></div>
              <a href="https://twitter.com/share" class="twitter-share-button" data-count="none"><small>Tweet</small></a>
              <div class="pin-btn"><a data-pin-do="buttonBookmark" null href="//www.pinterest.com/pin/create/button/"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a></div>             
            </div><!--end social-->
          </div>
          <div class="row medium-collapse slider-con vis-over"><!--Size Slider-->
            @if($product->options()->size()->count())
            <div class="small-12 medium-6 columns lp">
              @include('ecommerce::partials.size')
            </div>
            @endif
            @if($product->options()->variation()->count())
            <div class="small-12 medium-6 columns lp">
              @include('ecommerce::partials.variation')
            </div>
            @endif
          </div><!-- End Range Slider-->
          <div class="row medium-collapse">
            @if($product->options()->colour()->count())
            <div class="small-12 medium-6 columns rp">
              @include('ecommerce::partials.colour')
            </div>
            @endif
            <div class="small-12 medium-6 columns @if($product->options()->colour()->count()) lp @else rp @endif">
              <p class="option-label">Quantity</p>
              <div class="quantity">
                <button class="right add-quantity" title="Add One"> + </button>
                <button class="left remove-quantity" title="Remove One"> - </button>
                {{ Form::hidden('full_permalink', $product->full_permalink) }}
                {{ Form::number('quantity', '1', ['class' => 'amount-centerd', 'min' => '1', 'step' => '1']) }}
              </div>
            </div>
          </div>    
          <div data-alert class="alert-box alert no-stock" style="display: none;">
            There is currently no stock available for this product.
          </div>
          <div class="row collapse btn-row visible-for-medium-up effect buttons l-space">
            <div class="medium-12 large-6 large-push-6 columns effect buttons lp">
              @include('ecommerce::components.forms.svg_button', ['svg_class' => 'orange', 'button_class' => 'add-to-cart', 'title' => 'Add to Cart'])
            </div>
            <div class="medium-12 large-6 large-pull-6 columns rp">
              @include('ecommerce::components.forms.svg_button', ['svg_class' => 'black', 'button_class' => 'wishlist', 'title' => 'Add to Wishlist'])
            </div>
          </div>
        </div><!--end-cols-->
      </div><!--End Row--> 

      <div class="row btn-row visible-for-small-only effect buttons">
        @include('ecommerce::components.forms.svg_button', ['svg_class' => 'orange', 'button_class' => '', 'title' => 'Add to Cart'])
        @include('ecommerce::components.forms.svg_button', ['svg_class' => 'black', 'button_class' => 'wishlist', 'title' => 'Add to Wishlist'])
      </div>
      <div class="row small-collapse medium-uncollapse show-for-small-only"> 
        <div class="small-12 medium-6 columns">
          @if($product->related()->active()->count())
          <p class="titles text-center">RELATED <strong>PRODUCTS</strong></p>
          <div class="related">
            <div class="related-imgs">
              @foreach($product->related()->active()->get() as $related_product)
              <a href="/products/{{$related_product->full_permalink}}">
                <img src="{{ Config::get('ecommerce::product.cdn') . $related_product->images()->first()->src . Config::get('ecommerce::product.query') }}" alt="{{ $related_product->full_title }}" class="wow zoomIn" width="170" height="170">
              </a>
              @endforeach
            </div>
          </div><!--end related-->
          @endif
        </div> <!--end cols-->
      </div><!--end row-->
    {{ Form::close() }}
  </div> <!--end product-->
@stop

@section('inline_js')
<script>
  var stockControl = @if(Config::get('ecommerce::product.stock_control')) true @else false @endif ;

  if(!Modernizr.touch) {
    $(function() {
      $('.show-for-large-up .cycle-slide-active img').magnify();
    });

    $(document).on('cycle-after', '.show-for-large-up .cycle-slideshow', function() {
      $('.show-for-large-up .cycle-slide-active img').magnify();
    });
  }

  function no_stock() {
    $('.no-stock').show();
    $('.product .add-to-cart').prop('disabled', true);
  }

  $(document).on('click', '.colour [data-id]', function(e) {
    e.preventDefault();

    if($(this).is('.disabled')) {
      return;
    }

    $('#colour_id').val($(this).data('id'));
    $('.colour [data-id].cycle-pager-active').removeClass('cycle-pager-active');
    $(this).addClass('cycle-pager-active');

    var colour_id = $(this).data('id');

    $.get('/products/{{ $product->full_permalink }}/size/' + colour_id, function(res) {
      var active = JSON.parse(res);

      $('.size [data-id]').each(function() {
        if(active.indexOf(parseInt($(this).data('id'))) != -1) {
          if(stockControl) {
            $(this).removeClass('disabled');
          } else {
            $(this).addClass('disabled');
          }
        } else {
          if(stockControl) {
            $(this).addClass('disabled');
          } else {
            $(this).removeClass('disabled');
          }
        }
      });
    });

    $('.show-for-large-up .adv-custom-pager').find('[data-id=' + $(this).data('id') + ']').click();
    $('.hide-for-large-up .adv-custom-pager').find('[data-id=' + $(this).data('id') + ']').click();
  });

  $(document).on('click', '.size a', function(e) {
    e.preventDefault();

    if($(this).is('.disabled')) {
      return;
    }
    var size_id = $(this).data('id');

    $('#size_id').val(size_id);
    $('.size a').removeClass('active');
    $(this).addClass('active');

    $.get('/products/{{ $product->full_permalink }}/colour/' + size_id, function(res) {
      var active = JSON.parse(res);

      $('.colour [data-id]').each(function() {
        if(active.indexOf(parseInt($(this).data('id'))) != -1) {
          if(stockControl) {
            $(this).removeClass('disabled');
          } else {
            $(this).addClass('disabled');
          }
        } else {
          if(stockControl) {
            $(this).addClass('disabled');
          } else {
            $(this).removeClass('disabled');
          }
        }
      });
    });
  });

  $(document).on('click', '.add-quantity', function(e) {
    $quantity = $('.quantity input[type=number]');

    $quantity.val(parseInt($quantity.val()) + 1);
    
    e.preventDefault();
  });

  $(document).on('click', '.remove-quantity', function(e) {
    $quantity = $('.quantity input[type=number]');

    if($quantity.val() > 0) {
      $quantity.val(parseInt($quantity.val()) - 1);
    }
    
    e.preventDefault();
  });

  $(document).on('click', '.back', function(e) {
    window.history.back();

    e.preventDefault();
  });

  $(document).on('click', '.wishlist', function(e) {
    e.preventDefault();
    $('.product-form').attr('action', '/wishlist/product');
    $('.product-form').submit();
  });
  
  $(function() {
    //check if size and colour exist
    //see if any colours are active
    var size_id = $('#size_id').val(),
        colour_id = $('#colour_id').val(),
        colour_ex = $('#colour_id').get(0),
        size_ex = $('#size_id').get(0);

    if(size_ex && colour_ex) {
      $.get('/products/{{ $product->full_permalink }}/colour', function(res) {
        var active = JSON.parse(res);

        $('.colour [data-id]').each(function() {
          if(active.indexOf(parseInt($(this).data('id'))) != -1) {
            if(stockControl) {
              $(this).removeClass('disabled');
            } else {
              $(this).addClass('disabled');
            }
          } else {
            if(stockControl) {
              $(this).addClass('disabled');
            } else {
              $(this).removeClass('disabled');
            }
          }
        });

        var $colour = $('.colour [data-id]:not(.disabled):first');
        if($colour.get(0)) {
          $colour.addClass('cycle-pager-active');
          $('#colour_id').val($colour.data('id'));

          var colour_id = $colour.data('id');

          $.get('/products/{{ $product->full_permalink }}/size/' + colour_id, function(res) {
            var active = JSON.parse(res);

            $('.size [data-id]').each(function() {
              if(active.indexOf(parseInt($(this).data('id'))) != -1) {
                $(this).removeClass('disabled');
              } else {
                $(this).addClass('disabled');
              }
            });

            var $size = $('.size [data-id]:not(.disabled):first');
            if($size.get(0)) {
              $size.addClass('active');
              $('#size_id').val($size.data('id'));
            } else {
              no_stock();
            }
          });
        } else {
          no_stock();
        }
      });
    } else if(size_ex) {
      $.get('/products/{{ $product->full_permalink }}/size', function(res) {
        var active = JSON.parse(res);

        $('.size [data-id]').each(function() {
          if(active.indexOf(parseInt($(this).data('id'))) != -1) {
            if(stockControl) {
              $(this).removeClass('disabled');
            } else {
              $(this).addClass('disabled');
            }
          } else {
            if(stockControl) {
              $(this).addClass('disabled');
            } else {
              $(this).removeClass('disabled');
            }
          }
        });

        var $size = $('.size [data-id]:not(.disabled):first');
        if($size.get(0)) {
          $size.addClass('active');
          $('#size_id').val($size.data('id'));
        } else {
          no_stock();
        }
      });
    } else if(colour_ex) {
      $.get('/products/{{ $product->full_permalink }}/colour', function(res) {
        var active = JSON.parse(res);

        $('.colour [data-id]').each(function() {
          if(active.indexOf(parseInt($(this).data('id'))) != -1) {
            if(stockControl) {
              $(this).removeClass('disabled');
            } else {
              $(this).addClass('disabled');
            }
          } else {
            if(stockControl) {
              $(this).addClass('disabled');
            } else {
              $(this).removeClass('disabled');
            }
          }
        });

        var $colour = $('.colour [data-id]:not(.disabled):first');
        if($colour.get(0)) {
          $colour.addClass('cycle-pager-active');
          $('#colour_id').val($colour.data('id'));
        } else {
          no_stock();
        }
      });
    } else {
      $.get('/products/{{ $product->full_permalink }}/stock', function(res) {
        var active = JSON.parse(res);
       console.log(active); 
        if(stockControl && !active.length) {
          no_stock();
        }
      });
    }
  });
</script>
<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
@stop
