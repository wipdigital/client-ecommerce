@extends('ecommerce::layouts.standard')

@section('main')
  @include('ecommerce::components.navigation.checkout', ['active' => 'checkout'])

  <div class="row">
    <div class="small-12 columns medium-7 large-6 medium-centered shipping-checkout shipping-con">
        <h2>Choose a <strong>Delivery/Pickup</strong> <em>time:</em></h2>
    </div>
  </div>

  {{ Form::open(['url' => 'checkout/time', 'method' => 'POST', 'class' => 'checkout']) }}
  <div class="row medium-uncollapse shipping-con">
    <div class="small-12 medium-6 medium-offset-3 columns">
        <section class="selectors">
          {{ Form::label('timestamp', 'Time', ['class' => 'select-label']) }}
          {{ Form::select('time', $times) }}
        </section>
        @if($errors->has('timestamp'))
          <div class="alert-box">{{ $errors->first('timestamp') }}</div>
        @endif
    </div><!--End Col-->
  </div><!--End row-->

  {{ Form::hidden('redirect_uri', '') }}
  {{ Form::hidden('delivery_method', '') }}

  <div class="row collapse medium-uncollapse check">
    <div class="small-12 medium-3 medium-offset-3 columns">
        {{ Form::submit('Pickup', ['class' => 'button left secondary expand']) }}
    </div>
    <div class="small-12 medium-3 columns">
        {{ Form::submit('Delivery', ['class' => 'button right secondary expand']) }}
    </div>
  </div>
  {{ Form::close() }}
@stop

@section('inline_js')
  <script>
    $(document).on('click', '.pickup', function(e) {
      var $form = $(this).parents('form');
      e.preventDefault();

      $form.find('.delivery_method').val('pickup');
      $form.find('.redirect_uri').val('/checkout/summary');
      $form.submit();
    });

    $(document).on('click', '.delivery', function(e) {
      var $form = $(this).parents('form');
      e.preventDefault();

      $form.find('.delivery_method').val('delivery');
      $form.find('.redirect_uri').val('/checkout/shipping');
      $form.submit();
    });

    $(document).on('click', '.shipping-con #fb-auth', function(e) {
      document.cookie = "redirect_uri=/checkout/billing";
      window.location = 'https://www.facebook.com/dialog/oauth?client_id={{ Config::get('ecommerce::customer.facebook.id') }}&redirect_uri={{ Config::get('ecommerce::customer.facebook.redirect_uri') }}&response_type=code';
    });

    $(document).on('click', '.shipping-con #in-auth', function(e) {
      document.cookie = "redirect_uri=/checkout/billing";
      window.location = 'https://instagram.com/oauth/authorize/?client_id={{ Config::get('ecommerce::customer.instagram.id') }}&redirect_uri={{ Config::get('ecommerce::customer.instagram.redirect_uri') }}&response_type=code';
    });
  </script>
@stop
