@extends('ecommerce::layouts.standard')

@section('main')
  @include('ecommerce::components.navigation.checkout', ['active' => 'checkout'])

<div class="row">
  <div class="small-12 columns medium-7 large-6 medium-centered shipping-checkout shipping-con">
      <h2><strong>Log in</strong> <em>with:</em></h2>
  </div>
</div>

  <div class="row collapse medium-uncollapse shipping-con">
    <div class="small-6 medium-3 medium-offset-3 columns">
      <button id="fb-auth" class="button expand center" title="Log in with Facebook"><i class="fa fa-facebook"></i></button>
    </div>
    <div class="small-6 medium-3 columns">
      <button id="in-auth" class="button expand center" title="Log in with Instagram"><i class="fa fa-instagram"></i></button>
    </div>     
  </div>

  <div class="row medium-uncollapse shipping-con">
    <div class="small-12 columns medium-6 medium-offset-3">
      <h2><span>Or</span> <br><strong><em>single use:</em></strong></h2>
      <p>No details are stored.</p>

      {{ Form::open(['url' => 'checkout', 'method' => 'POST', 'class' => 'checkout']) }}
        <span class="input-k input--kozakura inner @if(Input::old('name')) input--filled @endif">
          {{ Form::text('name', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-name']) }}
          <label class="input__label input__label--kozakura inner" for="form-name">
            <span class="input__label-content input__label-content--kozakura" data-content="Name"><i class="fa fa-user"></i> Name</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('name'))
        <div class="alert-box">{{ $errors->first('name') }}</div>
        @endif

        <span class="input-k input--kozakura inner @if(Input::old('email')) input--filled @endif">
          {{ Form::email('email', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-email']) }}
          <label class="input__label input__label--kozakura inner" for="form-email">
            <span class="input__label-content input__label-content--kozakura" data-content="Email"><i class="fa fa-envelope"></i> Email</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('email'))
        <div class="alert-box">{{ $errors->first('email') }}</div>
        @endif

      </div><!--End Col-->
    </div><!--End row-->

    <div class="row collapse medium-uncollapse check">
      <div class="small-12 medium-3 medium-offset-3 columns">
        <a href="#" class="step-back button expand">Back</a>
      </div>
      <div class="small-12 medium-3 columns">
        {{ Form::submit('Log in', array('class' => 'button right secondary expand')) }}
      </div>
    </div>
  {{ Form::close() }}
@stop

@section('inline_js')
  <script>
    $(document).on('click', '.shipping-con #fb-auth', function(e) {
      document.cookie = "redirect_uri=/checkout/billing";
      window.location = 'https://www.facebook.com/dialog/oauth?client_id={{ Config::get('ecommerce::customer.facebook.id') }}&redirect_uri={{ Config::get('ecommerce::customer.facebook.redirect_uri') }}&response_type=code';
    });

    $(document).on('click', '.shipping-con #in-auth', function(e) {
      document.cookie = "redirect_uri=/checkout/billing";
      window.location = 'https://instagram.com/oauth/authorize/?client_id={{ Config::get('ecommerce::customer.instagram.id') }}&redirect_uri={{ Config::get('ecommerce::customer.instagram.redirect_uri') }}&response_type=code';
    });
  </script>
@stop
