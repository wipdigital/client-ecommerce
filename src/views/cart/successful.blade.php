@extends('ecommerce::layouts.standard')

@section('main')
  <h1>{{ $page->full_title }}</h1>
  <p>{{ $page->description }}</p>
@stop

