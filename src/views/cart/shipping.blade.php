@extends('ecommerce::layouts.standard')

@section('main')
  {{ Form::model($customer, ['url' => 'checkout/shipping', 'method' => 'POST', 'class' => 'shipping']) }}

    @include('ecommerce::components.navigation.checkout', ['active' => 'shipping'])

    <div class="row">
      <div class="small-12 medium-8 large-6 columns medium-centered shipping-con">
        <h2><strong>Delivery</strong> Details</h2>

        <div class="row">
          <div class="small-10 columns">
            <span class="right">
              Same as billing?
            </span>
          </div>
          <div class="small-2 columns">
            <div class="switch">
              <input id="same_as_billing" type="checkbox" name="same_as_billing">
              <label for="same_as_billing"></label>
            </div>
          </div>
        </div>

        <span class="input-k input--kozakura inner @if(Input::old('shipping_name')) input--filled @endif">
          {{ Form::text('shipping_name', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-name']) }}
          <label class="input__label input__label--kozakura inner" for="form-name">
            <span class="input__label-content input__label-content--kozakura" data-content="Name"><i class="fa fa-envelope"></i> Name</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('shipping_name'))
        <div class="alert-box">{{ $errors->first('shipping_name') }}</div>
        @endif

        <span class="input-k input--kozakura inner @if(Input::old('shipping_phone')) input--filled @endif">
          {{ Form::text('shipping_phone', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-phone']) }}
          <label class="input__label input__label--kozakura inner" for="form-phone">
            <span class="input__label-content input__label-content--kozakura" data-content="Phone"><i class="fa fa-phone"></i> Phone</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('shipping_phone'))
        <div class="alert-box">{{ $errors->first('shipping_phone') }}</div>
        @endif

        <span class="input-k input--kozakura inner @if(Input::old('shipping_address1')) input--filled @endif">
          {{ Form::text('shipping_address1', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-address']) }}
          <label class="input__label input__label--kozakura inner" for="form-address">
            <span class="input__label-content input__label-content--kozakura" data-content="Address&#42;"><i class="fa fa-home"></i> Address&#42;</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('shipping_address1'))
        <div class="alert-box">{{ $errors->first('shipping_address1') }}</div>
        @endif

        <span class="input-k input--kozakura inner @if(Input::old('shipping_address2')) input--filled @endif">
          {{ Form::text('shipping_address2', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-address2']) }}
          <label class="input__label input__label--kozakura inner" for="form-address2">
            <span class="input__label-content input__label-content--kozakura" data-content="Address line 2"><i class="fa fa-home"></i> Address line 2</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('shipping_address2'))
        <div class="alert-box">{{ $errors->first('shipping_address2') }}</div>
        @endif

        <span class="input-k input--kozakura inner @if(Input::old('shipping_suburb')) input--filled @endif">
          {{ Form::text('shipping_suburb', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-suburb']) }}
          <label class="input__label input__label--kozakura inner" for="form-suburb">
            <span class="input__label-content input__label-content--kozakura" data-content="Suburb"><i class="fa fa-home"></i> Suburb</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('shipping_suburb'))
        <div class="alert-box">{{ $errors->first('shipping_suburb') }}</div>
        @endif

        <span class="input-k input--kozakura inner @if(Input::old('shipping_city')) input--filled @endif">
          {{ Form::text('shipping_city', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-city']) }}
          <label class="input__label input__label--kozakura inner" for="form-city">
            <span class="input__label-content input__label-content--kozakura" data-content="City&#42;"><i class="fa fa-building"></i> City&#42;</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('shipping_city'))
        <div class="alert-box">{{ $errors->first('shipping_city') }}</div>
        @endif

        <div class="row">
          <div class="small-6 columns en-overflow">
            <section class="selectors">
              {{ Form::label('shipping_state', 'State', ['class' => 'select-label']) }}
              {{ Form::select('shipping_state', ['WA' => 'WA', 'ACT' => 'ACT', 'NSW' => 'NSW', 'NT' => 'NT', 'SA' => 'SA', 'VIC' => 'VIC', 'QLD' => 'QLD', 'TAS' => 'TAS'], null, ['class' => 'cs-select cs-skin-elastic']) }}
            </section>
            @if($errors->has('shipping_state'))
            <div class="alert-box">{{ $errors->first('shipping_state') }}</div>
            @endif
          </div>
          <div class="small-6 column">
            <span class="input-k input--kozakura inner @if(Input::old('shipping_postcode')) input--filled @endif">
              {{ Form::number('shipping_postcode', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-postcode']) }}
              <label class="input__label input__label--kozakura inner" for="form-postcode">
                <span class="input__label-content input__label-content--kozakura" data-content="Postcode"> Postcode</span>
              </label>
              <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
              </svg>
            </span>
            @if($errors->has('shipping_postcode'))
            <div class="alert-box">{{ $errors->first('shipping_postcode') }}</div>
            @endif
          </div>
        </div>
        {{ Form::hidden('shipping_country', 'Australia') }}
      </div><!--End col-->
    </div><!--End row-->

    <div class="row collapse medium-uncollapse check bottom-check">
      <div class="small-12 medium-4 large-3 medium-offset-2 large-offset-3 columns">
        <a href="/checkout/billing" class="button expand">Back</a>
      </div>
      <div class="small-12 medium-4 large-3 columns">
        {{ Form::submit('Checkout', array('class' => 'button right secondary expand')) }}
      </div>
    </div>
  {{ Form::close() }}
@stop

@section('inline_js')
<script>
//if there is a previous checked state then check or uncheck
$(function() {
  @if((Session::has('same_as_billing') && Session::get('same_as_billing') == true) || !Session::has('same_as_billing'))
    $('form.shipping .input__field, .cs-select').prop('disabled', true);
    $('#same_as_billing').prop('checked', true);
  @else
    $('form.shipping .input__field, .cs-select').prop('disabled', false);
    $('#same_as_billing').prop('checked', false);
  @endif
});

$(document).on('change', '#same_as_billing', function() {
  if($(this).prop('checked')) {
    $('form.shipping .input__field, .cs-select').prop('disabled', true);
  } else {
    $('form.shipping .input__field, .cs-select').prop('disabled', false);
  }
});
</script>
@stop
