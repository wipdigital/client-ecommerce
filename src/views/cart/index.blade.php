@extends('ecommerce::layouts.standard')

@section('main')

  <div class="row">  
  <div class="small-12 medium-8 large-6 columns medium-centered shipping-con">
  <h2><strong>Shopping</strong> Cart</h2>

  @if(count($cart->products))
  <table class="cart sum">
    <thead>
      <tr><th>Product</th><th>Qty</th><th>Price</th><th>Total</th></tr>
    </thead>
    <tbody>
    @foreach($cart->products as $product)
      <tr><td>
        <a href="/products/{{ $product->full_permalink }}">{{ $product->full_title }}</a>
        @if($product->pivot->options()->count())
          <small>
          @if($product->pivot->options()->variation()->count())
            @foreach($product->pivot->options()->variation()->get() as $option)
                {{ $option->title }}
              </a>
            @endforeach
            @if($product->pivot->options()->variation()->count())
            <a href="/products/options/{{ $product->pivot->id }}/edit">EDIT</a>
            @endif
          @endif
          @if($product->pivot->options()->colour()->count())
            {{ $product->pivot->options()->colour()->first()->title }}
          @endif
          @if($product->pivot->options()->size()->count())
            {{ $product->pivot->options()->size()->first()->title }}
          @endif
          </small>
        @endif
      </td><td>
        {{ Form::open(['url' => 'cart/product']) }}
          @if($product->pivot->options()->variation()->count())
            @foreach($product->pivot->options()->variation()->get() as $option)
              {{ Form::hidden('variation_id[]', $option->id) }}
            @endforeach
          @endif
          @if($product->pivot->options()->colour()->count())
            {{ Form::hidden('colour_id', $product->pivot->options()->colour()->first()->id) }}
          @endif
          @if($product->pivot->options()->size()->count())
            {{ Form::hidden('size_id', $product->pivot->options()->size()->first()->id) }}
          @endif
          {{ Form::number('quantity', $product->pivot->quantity, ['class' => 'quantity']) }}
          {{ Form::hidden('update_quantity', 1) }}
          {{ Form::hidden('full_permalink', $product->full_permalink) }}
        {{ Form::close() }}
      </td><td>${{ number_format($product->price + $product->pivot->price_offset, 2) }}</td><td>$<span class="subtotal">{{ number_format($product->pivot->quantity * ($product->price + $product->pivot->price_offset), 2) }}</span></td>
      </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr><td colspan="3"><span class="right">Sub Total<small> (Inc. GST)</small></span></td><td><span class="total right">${{ number_format(($cart->sub_total), 2) }}</span></td></tr>
        <tr><td colspan="3"><span class="right">GST</span></td><td><span class="gst right">${{ number_format($cart->gst, 2) }}</span></td></tr>
        @if($cart->total_shipping)
        <tr><td colspan="3"><span class="right">Shipping</span></td><td><span class="discount right">${{ number_format($cart->total_shipping, 2) }}</span></td></tr>
        @endif
        @if($cart->total_discount)
        <tr><td colspan="3"><span class="right">Discount</span></td><td><span class="discount right">-${{ number_format($cart->total_discount, 2) }}</span></td></tr>
        @endif
        @if($cart->voucher)
        <tr><td colspan="3"><span class="right">Voucher</span></td><td><span class="voucher right">-${{ number_format($cart->total_voucher, 2) }}</span></td></tr>
        @endif
        <tr><td colspan="3"><span class="right"><strong>Total</strong></span></td><td><span class="total right"><strong>${{ number_format(($cart->total), 2) }}</span></strong></td></tr>
    </tfoot>
  </table>
  @else
    <p>There are no products in your shopping cart.</p>
  @endif
  <div class="clearfix"></div>
  </div><!--End col-->
</div><!--End row-->

<div class="row collapse medium-uncollapse check bottom-check">
  <div class="small-12 medium-4 medium-offset-2 large-3 large-offset-3 columns">
    <a href="#" class="step-back button expand" title="Back to Shipping">Back</a>
  </div>
  <div class="small-12 medium-4 large-3 columns">
      @if(count($cart->products))
      <a href="/checkout" class="button right secondary expand">Checkout</a>
      @endif
  </div>
</div>






@stop

@section('inline_js')
<script>
  $(document).on('change', '.quantity', function() {
    $(this).parents('form').submit();
  });
</script>
@stop
