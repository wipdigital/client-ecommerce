@extends('ecommerce::layouts.standard')

@section('main')
  {{ Form::model($customer, ['url' => 'checkout/billing', 'method' => 'POST']) }}

    @include('ecommerce::components.navigation.checkout', ['active' => 'billing'])

    <div class="row"> 
      <div class="small-12 medium-8 large-6 columns medium-centered shipping-con">
        <h2><strong>Billing</strong> Details</h2>

        <span class="input-k input--kozakura inner @if(Input::old('name')) input--filled @endif">
          {{ Form::text('name', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-name']) }}
          <label class="input__label input__label--kozakura inner" for="form-name">
            <span class="input__label-content input__label-content--kozakura" data-content="Name"><i class="fa fa-envelope"></i> Name</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('name'))
        <div class="alert-box">{{ $errors->first('name') }}</div>
        @endif

        <span class="input-k input--kozakura inner @if(Input::old('email')) input--filled @endif">
          {{ Form::email('email', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-email']) }}
          <label class="input__label input__label--kozakura inner" for="form-email">
            <span class="input__label-content input__label-content--kozakura" data-content="Email"><i class="fa fa-envelope"></i> Email</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('email'))
        <div class="alert-box">{{ $errors->first('email') }}</div>
        @endif

        <span class="input-k input--kozakura inner @if(Input::old('phone')) input--filled @endif">
          {{ Form::text('phone', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-phone']) }}
          <label class="input__label input__label--kozakura inner" for="form-phone">
            <span class="input__label-content input__label-content--kozakura" data-content="Phone"><i class="fa fa-phone"></i> Phone</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('phone'))
        <div class="alert-box">{{ $errors->first('phone') }}</div>
        @endif

        <span class="input-k input--kozakura inner @if(Input::old('address1')) input--filled @endif">
          {{ Form::text('address1', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-address']) }}
          <label class="input__label input__label--kozakura inner" for="form-address">
            <span class="input__label-content input__label-content--kozakura" data-content="Address&#42;"><i class="fa fa-home"></i> Address&#42;</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('address1'))
        <div class="alert-box">{{ $errors->first('address1') }}</div>
        @endif

        <span class="input-k input--kozakura inner @if(Input::old('address2')) input--filled @endif">
          {{ Form::text('address2', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-address2']) }}
          <label class="input__label input__label--kozakura inner" for="form-address2">
            <span class="input__label-content input__label-content--kozakura" data-content="Address line 2"><i class="fa fa-home"></i> Address line 2</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('address2'))
        <div class="alert-box">{{ $errors->first('address2') }}</div>
        @endif

        <span class="input-k input--kozakura inner @if(Input::old('suburb')) input--filled @endif">
          {{ Form::text('suburb', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-suburb']) }}
          <label class="input__label input__label--kozakura inner" for="form-suburb">
            <span class="input__label-content input__label-content--kozakura" data-content="Suburb"><i class="fa fa-home"></i> Suburb</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('suburb'))
        <div class="alert-box">{{ $errors->first('suburb') }}</div>
        @endif

        <span class="input-k input--kozakura inner @if(Input::old('city')) input--filled @endif">
          {{ Form::text('city', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-city']) }}
          <label class="input__label input__label--kozakura inner" for="form-city">
            <span class="input__label-content input__label-content--kozakura" data-content="City&#42;"><i class="fa fa-building"></i> City&#42;</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('city'))
        <div class="alert-box">{{ $errors->first('city') }}</div>
        @endif

        <div class="row">
          <div class="small-6 columns en-overflow">
            <section class="selectors">
              {{ Form::label('state', 'State', ['class' => 'select-label']) }}
              {{ Form::select('state', ['WA' => 'WA', 'ACT' => 'ACT', 'NSW' => 'NSW', 'NT' => 'NT', 'SA' => 'SA', 'VIC' => 'VIC', 'QLD' => 'QLD', 'TAS' => 'TAS'], null, ['class' => 'cs-select cs-skin-elastic']) }}
            </section>
            @if($errors->has('state'))
            <div class="alert-box">{{ $errors->first('state') }}</div>
            @endif
          </div>
          <div class="small-6 column">
            <span class="input-k input--kozakura inner @if(Input::old('postcode')) input--filled @endif">
              {{ Form::number('postcode', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-postcode']) }}
              <label class="input__label input__label--kozakura inner" for="form-postcode">
                <span class="input__label-content input__label-content--kozakura" data-content="Postcode"> Postcode</span>
              </label>
              <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
              </svg>
            </span>
            @if($errors->has('postcode'))
            <div class="alert-box">{{ $errors->first('postcode') }}</div>
            @endif
          </div>         
        </div>
        {{ Form::hidden('country', 'Australia') }}
      </div><!--End col-->
    </div><!--End row-->

    <div class="row collapse medium-uncollapse check bottom-check">
      <div class="small-12 medium-4 large-3 medium-offset-2 large-offset-3 columns">
        <a href="/checkout" class="button expand">Back</a>
      </div>
      <div class="small-12 medium-4 large-3 columns">
        {{ Form::submit('Checkout', array('class' => 'button right secondary expand')) }}
      </div>
    </div>
  {{ Form::close() }}
@stop
