@extends('ecommerce::layouts.standard')

@section('main')
@include('ecommerce::components.navigation.checkout', ['active' => 'payment'])

<div class="row top-check">
  <div class="small-12 medium-8 large-6 columns medium-centered shipping-con">

    <h2><strong>Cart</strong> Summary</h2>

    @if(isset($checkout_errors))
    <div class="errors alert-box">
      <p>Sorry not all the items you have requested are available in that quantity of stock. Please review your order and checkout again.</p>
    </div>
    @endif

    @if(count($customer->cart->products))
    <table class="cart sum" width="100%">
      <thead>
        <tr><th>Product</th><th>Quantity</th><th>Price</th><th><span class="right">Total</span></th></tr>
      </thead>
      <tbody>
      @foreach($customer->cart->products as $product)
        <tr @if(isset(\Session::get('checkout_errors')[$product->pivot->id])) class="changed" @endif ><td>
          {{ $product->full_title }}
          @if($product->pivot->options()->count())
            <small>
            @if($product->pivot->options()->variation()->count())
              @foreach($product->pivot->options()->variation()->get() as $option)
                  {{ $option->title }}
                </a>
              @endforeach
            @endif
            @if($product->pivot->options()->colour()->count())
              {{ $product->pivot->options()->colour()->first()->title }}
            @endif
            @if($product->pivot->options()->size()->count())
              {{ $product->pivot->options()->size()->first()->title }}
            @endif
            </small>
          @endif
        </td><td>
          {{ $product->pivot->quantity }}
        </td><td>
          ${{ number_format($product->price + $product->pivot->price_offset, 2) }}
        </td><td>
          <span class="right">${{ number_format($product->pivot->quantity * ($product->price + $product->pivot->price_offset), 2) }}</span>
        </td></tr>
      @endforeach
      </tbody>
      <tfoot>
        <tr><td colspan="3"><span class="right">Sub Total<small> (Inc. GST)</small></span></td><td><span class="total right">${{ number_format(($customer->cart->sub_total), 2) }}</span></td></tr>
        <tr><td colspan="3"><span class="right">GST</span></td><td><span class="gst right">${{ number_format($customer->cart->gst, 2) }}</span></td></tr>
        @if($customer->cart->total_shipping)
        <tr><td colspan="3"><span class="right">Shipping</span></td><td><span class="discount right">${{ number_format($customer->cart->total_shipping, 2) }}</span></td></tr>
        @endif
        @if($customer->cart->discount_code)
        <tr><td colspan="3"><span class="right">Discount</span></td><td><span class="discount right">-${{ number_format($customer->cart->total_discount, 2) }}</span></td></tr>
        @endif
        @if($customer->cart->voucher)
        <tr><td colspan="3"><span class="right">Voucher</span></td><td><span class="voucher right">-${{ number_format($customer->cart->total_voucher, 2) }}</span></td></tr>
        @endif
        <tr><td colspan="3"><span class="right"><strong>Total</strong></span></td><td><span class="total right"><strong>${{ number_format(($customer->cart->total), 2) }}</span></strong></td></tr>
      </tfoot>
    </table>
    @else
      <p>There are no products in your shopping cart.</p>
    @endif

    {{ Form::model($cart->discount_code, array('url' => 'payment/discount', 'method' => 'POST', 'class' => 'checkout')) }}
      <span class="input-k input--kozakura inner">
        {{ Form::text('code', $customer->cart->voucher ? $customer->cart->voucher->code : ($customer->cart->discount_code ? $customer->cart->discount_code->code : null), ['class' => 'input__field input__field--kozakura inner btn-space-2', 'id' => 'discount']) }}    
        <label class="input__label input__label--kozakura inner" for="discount">
          <span class="input__label-content input__label-content--kozakura" data-content="Discount"><i class="fa fa-scissors"></i> Discount</span>
        </label>
        <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
          <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
        </svg>
      </span>

      @if($errors->has('code'))
      <div class="alert-box">{{ $errors->first('code') }}</div>
      @endif

      @if(!$customer->cart->total)
      <div class="alert-box">Your total order will be covered by your voucher. Please confirm your order before proceeding as it will be processed immediately.</div>
      @endif

      {{ Form::submit('Apply', array('class' => 'button right small discount')) }}
    {{ Form::close() }}
  </div>
</div>

@if($customer->cart->total)
  {{ Form::open(['url' => 'payment/creditcard', 'method' => 'post', 'class' => 'commbank', 'data-abide']) }}
    <div class="row">
      <div class="small-12 medium-8 large-6 columns medium-centered shipping-con">
        <h2><strong>Payment</strong> Details</h2>

          {{ Form::hidden('vpc_Version', '1') }}
          {{ Form::hidden('vpc_Command', 'pay') }}
          {{ Form::hidden('vpc_AccessCode', Config::get('ecommerce::commbank.access_code')) }}
          {{ Form::hidden('vpc_MerchTxnRef', $cart->id . rand(0,100)) }}
          {{ Form::hidden('vpc_Merchant', Config::get('ecommerce::commbank.merchant_id')) }}
          {{ Form::hidden('vpc_OrderInfo', 'Customer ' . $cart->customer_id) }}
          {{ Form::hidden('vpc_Amount', $cart->total*100) }}

          <div class="errors alert-box" style="display:none">
            <h3>Payment Error</h3>
            <p>There has been a problem processing your payment. Please check your card details and try again.</p>
          </div>

          <span class="input-k input--kozakura inner">
            <input class="input__field input__field--kozakura inner" type="text" id="vpc_CardNum" name="vpc_CardNum" required pattern="number">
            <label class="input__label input__label--kozakura inner" for="vpc_CardNum">
              <span class="input__label-content input__label-content--kozakura" data-content="Credit Card Number"><i class="fa fa-credit-card"></i> Credit Card Number</span>
            </label>
            <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
            </svg>
          </span>

          <span class="input-k input--kozakura inner">
            <input class="input__field input__field--kozakura inner" type="text" id="vpc_CardName" name="vpc_CardName" required>
            <label class="input__label input__label--kozakura inner" for="vpc_CardName">
              <span class="input__label-content input__label-content--kozakura" data-content="Name on Card"><i class="fa fa-user"></i> Name on Card</span>
            </label>
            <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
            </svg>
          </span>

          <div class="row">
            <div class="small-6 column">
              <section class="selectors">
                <label class="select-label" for="vpc_CardExpMonth">Expiry Month</label> 
                <select class="cs-select cs-skin-elastic" id="vpc_CardExpMonth" name="vpc_CardExpMonth" required>
                  <option value="" disabled selected>Select Month</option>
                  <option value="01">January</option>
                  <option value="02">February</option>
                  <option value="03">March</option>
                  <option value="04">April</option>
                  <option value="05">May</option>
                  <option value="06">June</option>
                  <option value="07">July</option>
                  <option value="08">August</option>
                  <option value="09">September</option>
                  <option value="10">October</option>
                  <option value="11">November</option>
                  <option value="12">December</option>
                </select>
              </section>
            </div>

            <div class="small-6 column">
              <section class="selectors">
                <label class="select-label" for="vpc_CardExpYear">Expiry Year</label>
                <select class="cs-select cs-skin-elastic" id="vpc_CardExpYear" name="vpc_CardExpYear" required>
                  <option value="" disabled selected>Select Year</option>
                  <option value="15">2015</option>
                  <option value="16">2016</option>
                  <option value="17">2017</option>
                  <option value="18">2018</option>
                  <option value="19">2019</option>
                  <option value="20">2020</option>
                  <option value="21">2021</option>
                  <option value="22">2022</option>
                  <option value="23">2023</option>
                  <option value="24">2024</option>
                  <option value="25">2025</option>
                </select>
              </section>
            </div>
          </div>    

          <div class="row">
            <div class="small-6 column right">
              <span class="input-k input--kozakura inner">
                <input class="input__field input__field--kozakura inner" type="text" id="vpc_CardSecurityCode" name="vpc_CardSecurityCode" required pattern="number">
                <label class="input__label input__label--kozakura inner" for="vpc_CardSecurityCode">
                  <span class="input__label-content input__label-content--kozakura" data-content="CVC"><i class="fa fa-lock"></i> CVC</span>
                </label>
                <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                </svg>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="row collapse medium-uncollapse check bottom-check">
        <div class="small-12 medium-4 medium-offset-2 large-3 large-offset-3 columns">
          <a href="/checkout/shipping" class="step-back button expand">Back</a>
        </div>
        <div class="small-12 medium-4 large-3 columns">
          <button type="submit" class="button right secondary expand">Pay Now</button>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  {{ Form::close() }}
@else
  <div class="row collapse medium-uncollapse check bottom-check">
    <div class="small-12 medium-4 medium-offset-2 large-3 large-offset-3 columns">
      <a href="/checkout/shipping" class="step-back button expand">Back</a>
    </div>
    <div class="small-12 medium-4 large-3 columns">
      {{ Form::open(['url' => '/payment/cod', 'class' => 'payment-card', 'method' => 'POST']) }}
        {{ Form::submit('Place Order', ['class' => 'button right secondary expand']) }}
      {{ Form::close() }}
    </div>
  </div>
@endif
@stop

@section('inline_js')
<script>
$(document).on('submit', '.commbank', function(e) {
  var $submit = $(this).find('[type=submit]');
  $submit.prop('disabled', true);
  $submit.html('<i class="fa fa-circle-o-notch fa-spin"></i> Pay Now');
  $.post($(this).attr('action'), $(this).serialize(), function(res) {
      var data = JSON.parse(res);

      switch(data.status) {
        case 'OK':
          window.location = '/payment/successful';
          break;

        default:
          $('.errors').slideDown();
          $submit.html('Pay Now');
          $submit.prop('disabled', false);

          break;
      }
  });

  e.preventDefault();
});

$(document).on('focus', '.commbank input, .commbank select', function() {
  $('.errors').slideUp();
});
</script>
@stop
