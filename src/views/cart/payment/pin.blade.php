@extends('ecommerce::layouts.standard')

@section('main')
  {{ Form::open(array('url' => 'payment/creditcard', 'method' => 'post', 'class' => 'pin')) }}
  @include('ecommerce::components.navigation.checkout', ['active' => 'payment'])

    <div class="row top-check">
      <div class="small-12 medium-8 large-6 columns medium-centered shipping-con">
        <h2><strong>Payment</strong> Details</h2>
          <div class="errors alert-box" style="display:none">
            <h3></h3>
            <ul></ul>
          </div>

          <input type="hidden" id="address-line1" value="{{ $cart->customer->address1 }}">
          <input type="hidden" id="address-line2" value="{{ $cart->customer->address2 }}">
          <input type="hidden" id="address-city" value="{{ $cart->customer->city }}">
          <input type="hidden" id="address-state" value="{{ $cart->customer->state }}">
          <input type="hidden" id="address-postcode" value="{{ $cart->customer->postcode }}">
          <input type="hidden" id="address-country" value="{{ $cart->customer->country }}">

          <span class="input-k input--kozakura inner">
            <input class="input__field input__field--kozakura inner" type="text" id="cc-number"/>
            <label class="input__label input__label--kozakura inner" for="cc-number">
              <span class="input__label-content input__label-content--kozakura" data-content="Credit Card Number"><i class="fa fa-credit-card"></i> Credit Card Number</span>
            </label>
            <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
            </svg>
          </span>

          <span class="input-k input--kozakura inner">
            <input class="input__field input__field--kozakura inner" type="text" id="cc-name"/>
            <label class="input__label input__label--kozakura inner" for="cc-name">
              <span class="input__label-content input__label-content--kozakura" data-content="Name on Card"><i class="fa fa-user"></i> Name on Card</span>
            </label>
            <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
            </svg>
          </span>

          <div class="row">
            <div class="small-6 column">
              <section class="selectors">
                <label class="select-label" for="cc-expiry-month">Expiry Month</label>
                <select class="cs-select cs-skin-elastic" id="cc-expiry-month">
                  <option value="" disabled selected>Select Month</option>
                  <option value="01">January</option>
                  <option value="02">February</option>
                  <option value="03">March</option>
                  <option value="04">April</option>
                  <option value="05">May</option>
                  <option value="06">June</option>
                  <option value="07">July</option>
                  <option value="08">August</option>
                  <option value="09">September</option>
                  <option value="10">October</option>
                  <option value="11">November</option>
                  <option value="12">December</option>
                </select>
              </section>
            </div>

            <div class="small-6 column">
              <section class="selectors">
                <label class="select-label" for="cc-expiry-year">Expiry Year</label>
                <select class="cs-select cs-skin-elastic" id="cc-expiry-year">
                  <option value="" disabled selected>Select Year</option>
                  <option value="15">2015</option>
                  <option value="16">2016</option>
                  <option value="17">2017</option>
                  <option value="18">2018</option>
                  <option value="19">2019</option>
                  <option value="20">2020</option>
                  <option value="21">2021</option>
                  <option value="22">2022</option>
                  <option value="23">2023</option>
                  <option value="24">2024</option>
                  <option value="25">2025</option>
                </select>
              </section>
            </div>
          </div>

          <div class="row">
            <div class="small-6 column right">
            <span class="input-k input--kozakura inner">
              <input class="input__field input__field--kozakura inner" type="text" id="cc-cvc"/>
              <label class="input__label input__label--kozakura inner" for="cc-cvc">
                <span class="input__label-content input__label-content--kozakura" data-content="CVC"><i class="fa fa-lock"></i> CVC</span>
              </label>
              <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
              </svg>
            </span>
            </div>
          </div>
      </div><!--End col-->
    </div><!--end-row-->

    <div class="row collapse medium-uncollapse check bottom-check">
      <div class="small-12 medium-4 medium-offset-2 large-3 large-offset-3 columns">
        <a href="/checkout/summary" class="step-back button expand">Back</a>
      </div>
      <div class="small-12 medium-4 large-3 columns">
        {{ Form::submit('Pay Now', ['class' => 'button right secondary expand']) }}
      </div>
    </div>
  {{ Form::close() }}
@stop


@section('inline_js')
  <script src='https://cdn.pin.net.au/pin.v2.js'></script>
  <script>
    // 1. Wait for the page to load
    $(function() {

      // 2. Create an API object with your publishable api key, and
      // specifying 'test' or 'live'.
      //
      // Be sure to use your live publishable key with the live api, and
      // your test publishable key with the test api.

      @if(App::environment('production'))
      var pinApi = new Pin.Api('{{ Config::get('ecommerce::payment.pin.publishable_key') }}');
      @else
      var pinApi = new Pin.Api('{{ Config::get('ecommerce::payment.pin.pubishable_key') }}', 'test');
      @endif

      var form = $('form.pin'),
          submitButton = form.find(":submit"),
          errorContainer = form.find('.errors'),
          errorList = errorContainer.find('ul'),
          errorHeading = errorContainer.find('h3');

      // 3. Add a submit handler to the form which calls Pin.js to
      // retrieve a card token, and then add that token to the form and
      // submit the form to your server.
      form.submit(function(e) {
        e.preventDefault();

        // Clear previous errors
        errorList.empty();
        errorHeading.empty();
        errorContainer.hide();

        // Disable the submit button to prevent multiple clicks
        submitButton.attr({disabled: true});

        // Fetch details required for the createToken call to Pin Payments
        var card = {
          number:           $('#cc-number').val(),
          name:             $('#cc-name').val(),
          expiry_month:     $('#cc-expiry-month').val(),
          expiry_year:      $('#cc-expiry-year').val(),
          cvc:              $('#cc-cvc').val(),
          address_line1:    $('#address-line1').val(),
          address_line2:    $('#address-line2').val(),
          address_city:     $('#address-city').val(),
          address_state:    $('#address-state').val(),
          address_postcode: $('#address-postcode').val(),
          address_country:  $('#address-country').val()
        };

        // Request a token for the card from Pin Payments
        pinApi.createCardToken(card).then(handleSuccess, handleError).done();
      });

      function handleSuccess(card) {
        // Add the card token to the form
        //
        // Once you have the card token on your server you can use your
        // private key and Charges API to charge the credit card.
        $('<input>')
          .attr({type: 'hidden', name: 'card_token'})
          .val(card.token)
          .appendTo(form);

        // Resubmit the form to the server
        //
        // Only the card_token will be submitted to your server. The
        // browser ignores the original form inputs because they don't
        // have their 'name' attribute set.
        form.get(0).submit();
      }

      function handleError(response) {
        errorHeading.text(response.error_description);

        if (response.messages) {
          $.each(response.messages, function(index, paramError) {
            $('<li>')
              .text(paramError.param + ": " + paramError.message)
              .appendTo(errorList);
          });
        }

        errorContainer.show();

        // Re-enable the submit button
        submitButton.removeAttr('disabled');
      };
    });
  </script>
@stop
