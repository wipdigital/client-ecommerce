@extends('ecommerce::layouts.standard')

@section('main')
  @include('ecommerce::components.navigation.checkout', ['active' => 'summary'])

<div class="row">  
  <div class="small-12 medium-8 large-6 columns medium-centered shipping-con">
    <h2><strong>Cart</strong> Summary</h2>

    @if(isset($quantity))
    <div class="errors alert-box">
      <p>Sorry not all the items you have requested are available in that quantity of stock. Please review your order and checkout again.</p>
    </div>
    @endif

    @if(count($customer->cart->products))
    <table class="cart sum" width="100%">
      <thead>
        <tr><th>Product</th><th>Quantity</th><th>Price</th><th><span class="right">Total</span></th></tr>
      </thead>
      <tbody>
      @foreach($customer->cart->products as $product)
        <tr><td>
          {{ $product->full_title }}
          @if($product->pivot->options()->count())
            <small>
            @if($product->pivot->options()->variation()->count())
              @foreach($product->pivot->options()->variation()->get() as $option)
                  {{ $option->title }}
                </a>
              @endforeach
            @endif
            @if($product->pivot->options()->colour()->count())
              {{ $product->pivot->options()->colour()->first()->title }}
            @endif
            @if($product->pivot->options()->size()->count())
              {{ $product->pivot->options()->size()->first()->title }}
            @endif
            </small>
          @endif
        </td><td>
          {{ $product->pivot->quantity }}
        </td><td>
          ${{ number_format($product->price + $product->pivot->price_offset, 2) }}
        </td><td>
          <span class="right">${{ number_format($product->pivot->quantity * ($product->price + $product->pivot->price_offset), 2) }}</span>
        </td></tr>
      @endforeach
      </tbody>
      <tfoot>
        <tr><td colspan="3"><span class="right">Sub Total<small> (Inc. GST)</small></span></td><td><span class="total right">${{ number_format(($customer->cart->sub_total), 2) }}</span></td></tr>
        <tr><td colspan="3"><span class="right">GST</span></td><td><span class="gst right">${{ number_format($customer->cart->gst, 2) }}</span></td></tr>
        @if($customer->cart->total_shipping)
        <tr><td colspan="3"><span class="right">Shipping</span></td><td><span class="discount right">${{ number_format($customer->cart->total_shipping, 2) }}</span></td></tr>
        @endif
        @if($customer->cart->discount_code)
        <tr><td colspan="3"><span class="right">Discount</span></td><td><span class="discount right">-${{ number_format($customer->cart->total_discount, 2) }}</span></td></tr>
        @endif
        @if($customer->cart->voucher)
        <tr><td colspan="3"><span class="right">Voucher</span></td><td><span class="voucher right">-${{ number_format($customer->cart->total_voucher, 2) }}</span></td></tr>
        @endif
        <tr><td colspan="3"><span class="right"><strong>Total</strong></span></td><td><span class="total right"><strong>${{ number_format(($customer->cart->total), 2) }}</span></strong></td></tr>
      </tfoot>
    </table>
    @else
      <p>There are no products in your shopping cart.</p>
    @endif

    {{ Form::model($discount, array('url' => 'checkout/summary', 'method' => 'POST', 'class' => 'checkout')) }}
      <span class="input-k input--kozakura inner">
        {{ Form::text('code', $customer->cart->voucher ? $customer->cart->voucher->code : ($customer->cart->discount_code ? $customer->cart->discount_code->code : null), ['class' => 'input__field input__field--kozakura inner btn-space-2', 'id' => 'discount']) }}    
        <label class="input__label input__label--kozakura inner" for="discount">
          <span class="input__label-content input__label-content--kozakura" data-content="Discount"><i class="fa fa-scissors"></i> Discount</span>
        </label>
        <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
          <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
        </svg>
      </span>

      @if($errors->has('code'))
      <div class="alert-box">{{ $errors->first('code') }}</div>
      @endif

      @if(!$customer->cart->total)
      <div class="alert-box">Your total order will be covered by your voucher. Please confirm your order before proceeding as it will be processed immediately.</div>
      @endif

      {{ Form::submit('Apply', array('class' => 'button right small discount')) }}
    {{ Form::close() }}

    <div class="clearfix"></div>
  </div><!--End col-->
</div><!--End row-->

<div class="row collapse medium-uncollapse check bottom-check">
  <div class="small-12 medium-4 medium-offset-2 large-3 large-offset-3 columns">
    <a href="/checkout/shipping" class="step-back button expand" title="Back to Shipping">Back</a>
  </div>
  <div class="small-12 medium-4 large-3 columns">
     @include('ecommerce::partials.payment')
  </div>
</div>
@stop
