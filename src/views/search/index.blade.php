@extends('ecommerce::layouts.standard')

@section('main')
  {{ Form::open(['url' => '/search', 'method' => 'post']) }}

    <span class="input-k input--kozakura inner @if(Input::old('query')) input--filled @endif">
      {{ Form::text('query', $query, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-query']) }}
      <label class="input__label input__label--kozakura inner" for="form-query">
        <span class="input__label-content input__label-content--kozakura" data-content="Query"><i class="fa fa-envelope"></i> Query</span>
      </label>
      <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
        <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
      </svg>
    </span>
    @if($errors->has('query'))
    <div class="alert-box">{{ $errors->first('query') }}</div>
    @endif

    {{ Form::submit('Search', ['class' => 'button tiny']) }}
  {{ Form::close() }}

  @if($query)
  <section class="ecommerce__category">
    @if(count($search_results))
    @include('ecommerce::components.products', ['products' => $search_results])
    @else
      <p>No search results found.</p>
    @endif
  </section>
  @endif
@stop
