@extends('ecommerce::layouts.standard')

@section('main')
  @include('ecommerce::components.navigation.breadcrumbs', ['resource' => $category])

  <section class="ecommerce__category">
    @include('ecommerce::components.categories', ['categories' => $category->categories()->active()->get()])

    @include('ecommerce::components.products', ['products' => $category->products()->active()->get()])
  </section>
@stop
