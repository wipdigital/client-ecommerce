<div class="variation options">
  <p class="option-label">Select Variations</p>
  @if($product->options()->variation()->count()) 
    @foreach($product->options()->variation()->get() as $option)
      @if(isset($product->pivot) && $product->pivot->options->contains($option->id))
        {{ Form::checkbox('variation_id[]', $option->id, true) }}
      @else
        {{ Form::checkbox('variation_id[]', $option->id) }}
      @endif
      {{ $option->title }}
    @endforeach
  @else
    <a href="#" title="One size fits most" class="active">A</a>
  @endif
</div>
