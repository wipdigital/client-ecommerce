@if($customer->cart->total)
  <div class="payment-card">
    <a href="/payment/creditcard" class="button expand">Pay Now</a>
  </div>
@else
  {{ Form::open(['url' => '/payment/cod', 'class' => 'payment-card', 'method' => 'POST']) }}
    {{ Form::submit('Place Order', ['class' => 'button expand']) }}
  {{ Form::close() }}
@endif
