@extends('ecommerce::layouts.email')

@section('main')
  <h1>Online Order Confirmation</h1>

  <p>Dear {{ $cart->customer->name }},</p>

  <p>You have recently purchased a voucher code online. Here is your code:</p>
  <p>{{ $voucher->code }}</p>
@stop
