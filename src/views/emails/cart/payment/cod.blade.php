@extends('ecommerce::layouts.email')

@section('main')
  <h1>Online Order Confirmation</h1>

  <p>Dear {{ $cart->customer->name }},</p>
  <p>This is to confirm a recent order you have made.</p>

  <table class="cart">
    <thead>
      <tr><th>Product</th><th>Quantity</th><th>Price</th><th>Total</th></tr>
    </thead>
    <tbody>
    @foreach($cart->products as $product)
      <tr><td>
        {{ $product->title }}
        @if($product->options()->count())
          <small>(
          @if($product->options()->colour()->count())
            Colour: {{ $product->options()->colour()->first()->title }}
          @endif
          @if($product->options()->size()->count())
            Size: {{ $product->options()->size()->first()->title }}
          @endif
          )</small>
        @endif
      </td><td>{{ $product->quantity }}</td><td>${{ number_format($product->price + $product->pivot->price_offset, 2) }}</td><td>${{ number_format($product->pivot->quantity * ($product->price + $product->pivot->price_offset), 2) }}</td></tr>
    @endforeach
    </tbody>
    <tfoot>
      <tr><td colspan="3"><span class="right">Total<small>(Inc. GST)</small></span></td><td>$<span class="total">{{ number_format(($cart->sub_total), 2) }}</span></td></tr>
      @if($cart->total_shipping)
      <tr><td colspan="3"><span class="right">Shipping</span></td><td>$<span class="discount">{{ number_format($cart->total_shipping, 2) }}</span></td></tr>
      @endif
      @if($cart->discount_code)
      <tr><td colspan="3"><span class="right">Discount</span></td><td>-$<span class="discount">{{ number_format($cart->total_discount, 2) }}</span></td></tr>
      @endif
      <tr><td colspan="3"><span class="right">GST</span></td><td>$<span class="gst">{{ number_format($cart->gst, 2) }}</span></td></tr>
      <tr><td colspan="3"><span class="right">Total</span></td><td>$<span class="total">{{ number_format(($cart->total), 2) }}</span></td></tr>
    </tfoot>
  </table>
@stop
