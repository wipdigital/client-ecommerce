@extends('ecommerce::layouts.standard')

@section('main')
  <h1>Wish List</h1>

  @if($wishlist)
    @if(count($wishlist->products))
    <table class="wishlist">
      <thead>
        <tr><th>Product</th><th>Price</th><th>&nbsp;</th></tr>
      </thead>
      <tbody>
      @foreach($wishlist->products as $product)
        <tr><td>
          <a href="/products/{{ $product->full_permalink }}">{{ $product->full_title }}</a>
          @if($product->pivot->options()->count())
            <small>
            @if($product->pivot->options()->colour()->count())
              {{ $product->pivot->options()->colour()->first()->title }}
            @endif
            @if($product->pivot->options()->size()->count())
              {{ $product->pivot->options()->size()->first()->title }}
            @endif
            </small>
          @endif
        </td><td>
          ${{ number_format($product->price, 2) }}
        </td><td>
          {{ Form::open(['url' => 'wishlist/product']) }}
            {{ Form::hidden('quantity', 0) }}
            {{ Form::hidden('full_permalink', $product->full_permalink) }}
            @if($product->pivot->options()->colour()->count())
              {{ Form::hidden('colour_id', $product->pivot->options()->colour()->first()->id) }}
            @endif
            @if($product->pivot->options()->size()->count())
              {{ Form::hidden('size_id', $product->pivot->options()->size()->first()->id) }}
            @endif
            <button type="submit" class="tiny"><i class="fa fa-close"></i></button>
          {{ Form::close() }}

          {{ Form::open(['url' => 'wishlist/cart']) }}
            {{ Form::hidden('quantity', 1) }}
            @if($product->pivot->options()->colour()->count())
              {{ Form::hidden('colour_id', $product->pivot->options()->colour()->first()->id) }}
            @endif
            @if($product->pivot->options()->size()->count())
              {{ Form::hidden('size_id', $product->pivot->options()->size()->first()->id) }}
            @endif
            {{ Form::hidden('full_permalink', $product->full_permalink) }}
            <button type="submit">Add to Cart</button>
          {{ Form::close() }}
        </td></tr>
      @endforeach
      </tbody>
    </table>
    @else
      <p>There are no products in your wish list.</p>
    @endif
  @else
    <p>You need to login to view your wishlist.</p>
  @endif

  <div class="row">
    <div class="small-6 columns">
      <a href="/products" class="button">Back</a>
    </div>
  </div>
@stop
