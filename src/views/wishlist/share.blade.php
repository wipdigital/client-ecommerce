@extends('ecommerce::layouts.master')

@section('main')

  <h1>{{ $wishlist->customer->name }}</h1>

  @if(count($wishlist->products))
  <div class="products">
    @include('ecommerce::components.products', ['products' => $wishlist->products])
  </div>
  @endif
@stop
