@if(count($products))
<div class="ecommerce__products">
  <div class="row">
  @foreach($products as $product)
    <div class="small-6 medium-3 columns">
      <a href="/products/{{ $product->full_permalink }}" title="{{ $product->title }}" class="category-card">
        <div class="background">
          <img src="{{ $product->images()->first() ? Config::get('ecommerce::product.cdn') . $product->images()->first()->src . Config::get('ecommerce::product.query') : '' }}" alt="{{ $product->title }}" class="zoomIn wow" width="300" height="300">
        </div>

        <div class="overlay">
          {{ $product->full_title }}
          @if($product->price) <div class="price">${{ $product->price }}</div> @endif
        </div>
      </a>
    </div>
  @endforeach
  </div>
</div>
@endif
