<div class="row navigation__checkout">
  <div class="small-12 columns medium-7 large-6 medium-centered shipping-checkout shipping-con">
    <div class="columns small-3">
      <a href="/checkout" class="fa-stack fa-lg disabled @if($active =='checkout') active @endif" title="Step One - Log In"><i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-user fa-stack-1x"></i><br><span>1. Log In</span></a>
    </div>
    <div class="columns small-3">
      <a href="/checkout/billing" class="fa-stack fa-lg disabled @if($active == 'billing') active @endif" title="Step Two - Billing Details"><i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-home fa-stack-1x"></i><br><span>2. Billing</span></a>
    </div>       
    <div class="columns small-3">
      <a href="/checkout/shipping" class="fa-stack fa-lg disabled @if($active == 'shipping') active @endif" title="Step Two - Delivery Details"><i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-truck fa-stack-1x"></i><br><span>3. Delivery</span></a>
    </div>
    <div class="columns small-3">
      <a href="/payment/creditcard" class="fa-stack fa-lg disabled @if($active == 'payment') active @endif" title="Step Four - Payment"><i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-credit-card fa-stack-1x"></i><br><span>4. Pay</span></a>
    </div>
  </div>
</div><!--End Checkout Nav Row-->    
