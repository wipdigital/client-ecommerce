<div class="navigation__breadcrumbs">
  <nav class="breadcrumbs" role="menubar" aria-label="breadcrumbs">
    <a href="/">Home</a>
    @if(count($resource->breadcrumbs))
    @foreach($resource->breadcrumbs as $category)
    <a href="/products/{{ $category->product_category_id ? $category->full_permalink : '' }}">{{ $category->full_title}}</a>
    @endforeach
    @endif
    <a href="/products/{{ $resource->product_category_id ? $resource->full_permalink : '' }}" class="current">{{ $resource->full_title }}</a>
  </nav>
</div>
