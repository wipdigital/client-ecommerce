<button class="button button--line button--effect-1 {{ $button_class }}">
  <span class="morph-shape" data-morph-active="M286,113c0,0-68.8-5-136-5c-78.2,0-137,5-137,5s5-15.802,5-50.5C18,33.999,13,12,13,12s59,5,137,5c85,0,136-5,136-5s-5,17.598-5,52C281,96.398,286,113,286,113z">
    <svg width="100%" height="100%" viewBox="0 0 300 125" preserveAspectRatio="none" class="{{ $svg_class }}">
      <path d="M286,113c0,0-68.8,0-136,0c-78.2,0-137,0-137,0s0-15.802,0-50.5C13,33.999,13,12,13,12s59,0,137,0c85,0,136,0,136,0s0,17.598,0,52C286,96.398,286,113,286,113z"/>
    </svg>
  </span>
  <span class="button__text">{{ $title }}</span>
</button>
