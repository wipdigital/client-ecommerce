<?php

Route::get('/kounta', 'WorkInProgress\ClientEcommerce\PaymentController@Kounta');

Route::get('/products', 'WorkInProgress\ClientEcommerce\ProductsController@getProduct');
Route::get('/products/tags/{tag}', 'WorkInProgress\ClientEcommerce\TagController@show');
Route::get('/products/{full_permalink}/stock/{quantity?}', 'WorkInProgress\ClientEcommerce\ProductOptionStockController@getStockFromProduct')->where('full_permalink', '(.*)');
Route::get('/products/{full_permalink}/size/{id?}/{quantity?}', 'WorkInProgress\ClientEcommerce\ProductOptionStockController@getSizeFromColour')->where('full_permalink', '(.*)');
Route::get('/products/{full_permalink}/colour/{id?}/{quantity?}', 'WorkInProgress\ClientEcommerce\ProductOptionStockController@getColourFromSize')->where('full_permalink', '(.*)');
Route::get('/products/options/{id}/edit', 'WorkInProgress\ClientEcommerce\ProductsController@editProduct');
Route::get('/products/{full_permalink}', 'WorkInProgress\ClientEcommerce\ProductsController@getProduct')->where('full_permalink', '(.*)');

Route::get('/cart/last', 'WorkInProgress\ClientEcommerce\CartController@getLast');
Route::controller('/cart', 'WorkInProgress\ClientEcommerce\CartController');
Route::get('/wishlist/{id}', 'WorkInProgress\ClientEcommerce\WishlistController@getShare');
Route::controller('/wishlist', 'WorkInProgress\ClientEcommerce\WishlistController');
Route::controller('/checkout', 'WorkInProgress\ClientEcommerce\CustomerController');

Route::get('/payment/creditcard', 'WorkInProgress\ClientEcommerce\PaymentController@get' . Config::get('ecommerce::payment.method'));
Route::post('/payment/creditcard', 'WorkInProgress\ClientEcommerce\PaymentController@post' . Config::get('ecommerce::payment.method'));
Route::controller('/payment', 'WorkInProgress\ClientEcommerce\PaymentController');

Route::get('/search', 'WorkInProgress\ClientEcommerce\ProductsController@getSearch');
Route::post('/search', 'WorkInProgress\ClientEcommerce\ProductsController@postSearch');

Route::get('/logout', 'WorkInProgress\ClientEcommerce\CustomerController@getLogout');

Route::post('/stock', 'WorkInProgress\ClientEcommerce\ProductOptionStockController@getStockFromProductOption');
Route::post('/reserved/{product_id}', 'WorkInProgress\ClientEcommerce\ProductOptionStockController@getReserved');
Route::get('/timeout', 'WorkInProgress\ClientEcommerce\CartController@getTimeout');

Route::get('/broken', 'WorkInProgress\ClientEcommerce\PaymentController@getBrokenCart');
?>
