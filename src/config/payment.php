<?php namespace WorkInProgress\ClientEcommerce;

return [
  'method' => 'Pin',

  'pin' => [
    'secret_key' => '',
    'publishable_key' => '',

    'charge' => [
      'description' => 'Online Order',
      'currency' => 'AUD'
    ]
  ],

  'email' => [
    'subject' => 'Online Order Confirmation',
    'voucher_subject' => 'Online Voucher Code'
  ]

];
