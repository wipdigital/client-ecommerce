<?php namespace WorkInProgress\ClientEcommerce;

return [
  'method' => 'Pin',

  'pin' => [
    'secret_key' => '24ji9JpAjW_QC2xFMmsODw',
    'publishable_key' => 'pk_9ObHVOaiK-3yorzpZ-sFqg',

    'charge' => [
      'description' => 'Online Order',
      'currency' => 'AUD'
    ]
  ],

  'email' => [
    'subject' => 'Online Order Confirmation',
    'voucher_subject' => 'Online Voucher Code'
  ]

];
