<?php namespace WorkInProgress\ClientEcommerce;

return [
  'facebook' => [
    'redirect_uri' => 'http://client.s.wip.technology/checkout/facebook'
  ],
  'instagram' => [
    'redirect_uri' => 'http://client.s.wip.technology/checkout/facebook'
  ],

  'validation' => [
    'billing' => [
      'name' => 'required',
      'email' => 'required|email',
      'phone' => 'required|digits:10',
      'address1' => 'required',
      'suburb' => 'required',
      'city' => 'required',
      'state' => 'required',
      'postcode' => 'required|digits:4',
      'country' => 'required'
    ],
    'shipping' => [
      'shipping_name' => 'required',
      'shipping_phone' => 'required|digits:10',
      'shipping_address1' => 'required',
      'shipping_suburb' => 'required',
      'shipping_city' => 'required',
      'shipping_state' => 'required',
      'shipping_postcode' => 'required|digits:4',
      'shipping_country' => 'required'
    ],
    'voucher' => [
      'code' => 'required|exists:vouchers,code,deleted_at,NULL,active,1'
    ],
    'discount' => [
      'code' => 'required|exists:discount_codes,code,deleted_at,NULL,active,1'
    ]

  ]

];
