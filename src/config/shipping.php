<?php namespace WorkInProgress\ClientEcommerce;

return [
  'total_limit' => 75.00,
  'fixed_cost' => 10.00,

  'location_control' => false
];
