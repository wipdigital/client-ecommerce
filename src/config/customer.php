<?php namespace WorkInProgress\ClientEcommerce;

return [
  'facebook' => [
    'id' => '522922637872098',
    'secret' => '8e54d131d7e15e21c76470159a661dac',
    'redirect_uri' => 'http://client.wip.technology/checkout/facebook'
  ],
  'instagram' => [
    'id' => '11e2cab678b94d73972eda5d210b8ad4',
    'secret' => '415d23f3ad134cd79be746974b09ec26',
    'redirect_uri' => 'http://client.wip.technology/checkout/instagram'
  ],

  'validation' => [
    'billing' => [
      'name' => 'required',
      'email' => 'required|email',
      'phone' => 'required|digits:10',
      'address1' => 'required',
      'suburb' => 'required',
      'city' => 'required',
      'state' => 'required',
      'postcode' => 'required|digits:4',
      'country' => 'required'
    ],
    'shipping' => [
      'shipping_name' => 'required',
      'shipping_phone' => 'required|digits:10',
      'shipping_address1' => 'required',
      'shipping_suburb' => 'required',
      'shipping_city' => 'required',
      'shipping_state' => 'required',
      'shipping_postcode' => 'required|digits:4',
      'shipping_country' => 'required'
    ],
    'voucher' => [
      'code' => 'required|exists:vouchers,code,deleted_at,NULL,active,1'
    ],
    'discount' => [
      'code' => 'required|exists:discount_codes,code,deleted_at,NULL,active,1'
    ]

  ]

];
