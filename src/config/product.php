<?php namespace WorkInProgress\ClientEcommerce;

return [
  'cdn' => 'http://platform.wip.technology',
  'query' => '',

  'stock_control' => false,
  'vintage_timeout' => 1
];
