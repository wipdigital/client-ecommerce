<?php namespace WorkInProgress\ClientEcommerce;

return [
  'inventory_asset_account_code' => 630,
  'cost_of_goods_sold_account_code' => 310,
  'sales_account_code' => 200,
  'inventory_adjustment_account_code' => 300,
  'prepayments_account_code' => 130
];

