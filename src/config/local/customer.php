<?php namespace WorkInProgress\ClientEcommerce;

return [
  'facebook' => [
    'redirect_uri' => 'http://client.l.wip.technology:8000/checkout/facebook'
  ],
  'instagram' => [
    'redirect_uri' => 'http://client.l.wip.technology:8000/checkout/instagram'
  ],

  'validation' => [
    'billing' => [
      'name' => 'required',
      'email' => 'required|email',
      'phone' => 'required|digits:10',
      'address1' => 'required',
      'suburb' => 'required',
      'city' => 'required',
      'state' => 'required',
      'postcode' => 'required|digits:4',
      'country' => 'required'
    ],
    'shipping' => [
      'shipping_name' => 'required',
      'shipping_phone' => 'required|digits:10',
      'shipping_address1' => 'required',
      'shipping_suburb' => 'required',
      'shipping_city' => 'required',
      'shipping_state' => 'required',
      'shipping_postcode' => 'required|digits:4',
      'shipping_country' => 'required'
    ],
    'voucher' => [
      'code' => 'required|exists:vouchers,code,deleted_at,NULL,active,1'
    ],
    'discount' => [
      'code' => 'required|exists:discount_codes,code,deleted_at,NULL,active,1'
    ]

  ]

];
